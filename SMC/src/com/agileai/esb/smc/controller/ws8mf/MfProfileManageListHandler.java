package com.agileai.esb.smc.controller.ws8mf;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;
import com.agileai.esb.component.manager.MessageFlowManager;
import com.agileai.esb.smc.bizmoduler.ws8mf.MfProfileManage;
import com.agileai.esb.smc.service.SoakerManage;
import com.agileai.esb.smc.util.SMCHelper;

public class MfProfileManageListHandler
        extends StandardListHandler {
	public static String ENABLE = "enable";
	public static String STOP = "stop";
	public static String STARTMF = "startMF";
	public static String STOPMF = "stopMF";
	public static String DODELETEREQ = "doDeleteReq";
    public MfProfileManageListHandler() {
        super();
        this.editHandlerClazz = MfProfileManageEditHandler.class;
        this.serviceId = buildServiceId(MfProfileManage.class);
    }
    
	public ViewRenderer doDeleteAction(DataParam param){
		String mfProfileId = param.get("MF_ID");
		String appId = param.get("APP_ID");
		SoakerManage soakerManage = SMCHelper.getSoakerManage();
		DataRow row = getService().getRecord(param);
		String appName = row.stringValue("APP_NAME");
		String messageFlowName = row.stringValue("MF_NAME");
		soakerManage.undeployMessageFlowProfile(appName, messageFlowName, mfProfileId);
		soakerManage.deleteMessageFlowProfile(appId,mfProfileId);
		storeParam(param);
		this.setAttribute("refreshTree","Y");
		return prepareDisplay(param);
	}
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		String mfState = param.get("mfState");
		List<DataRow> rsList = getService().findRecords(param);
		List<DataRow> records = new ArrayList<DataRow>();
		for (int i=0;i < rsList.size();i++){
			DataRow row = rsList.get(i);
			String appName = row.stringValue("APP_NAME");
			MessageFlowManager messageFlowManager = MessageFlowManager.instance(appName);
			String messageFlowId = row.stringValue("MF_ID");
			if (messageFlowManager.isActive(messageFlowId)){
				row.put("isActiveCode","true");
				row.put("isActive","已启动");
				if(ENABLE.equals(mfState)){
					records.add(row);
				}
			}else{
				row.put("isActiveCode","false");
				row.put("isActive","已停止");
				if(STOP.equals(mfState)){
					records.add(row);
				}
			}
			if(StringUtil.isNullOrEmpty(mfState)){
				records.add(row);
			}
		}
		this.setRsList(records);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
    @PageAction
    public ViewRenderer startMF(DataParam param){
    	ViewRenderer result = null;
    	
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
    		String id = idArray[i];
        	DataParam idParam = new DataParam();
        	String appId = param.get("appId");
        	idParam.put("MF_ID", id);
        	idParam.put("APP_ID", appId);
        	DataRow record = getService().getRecord(idParam);
        	String appName = record.stringValue("APP_NAME");
        	String messageFlowName = record.stringValue("MF_NAME");
    		try {
            	SoakerManage soakerManage = SMCHelper.getSoakerManage();
            	soakerManage.startMessageFlowProfile(appName, messageFlowName);
            	result = new AjaxRenderer(SUCCESS);
    		} catch (Exception e) {
    			result = new AjaxRenderer(FAIL);
    		}
    	}
    	return result;
    }
    
    @PageAction
    public ViewRenderer stopMF(DataParam param){
    	ViewRenderer result = null;
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
    		String id = idArray[i];
        	DataParam idParam = new DataParam();
        	String appId = param.get("appId");
        	idParam.put("MF_ID", id);
        	idParam.put("APP_ID", appId);
        	DataRow record = getService().getRecord(idParam);
        	String appName = record.stringValue("APP_NAME");
        	String messageFlowName = record.stringValue("MF_NAME");
    		try {
            	SoakerManage soakerManage = SMCHelper.getSoakerManage();
            	soakerManage.stopMessageFlowProfile(appName, messageFlowName);
            	result = new AjaxRenderer(SUCCESS);
    		} catch (Exception e) {
    			result = new AjaxRenderer(FAIL);
    		}
    	}
    	return result;
    }
    
	public ViewRenderer doQueryAction(DataParam param){
		return prepareDisplay(param);
	}
    
    protected void processPageAttributes(DataParam param) {
        initMappingItem("SHARELOG",
                        FormSelectFactory.create("BOOL_DEFINE").getContent());
        initMappingItem("DEPLOYED",
                        FormSelectFactory.create("BOOL_DEFINE").getContent());
        initMappingItem("START_WITH_SERVER",
                        FormSelectFactory.create("BOOL_DEFINE").getContent());
        
        setAttribute("mfState",
                FormSelectFactory.create("MfState").addSelectedValue(param.get("mfState")));
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "groupId", "");
        initParamItem(param, "appId", "");
    }
    
    public  ViewRenderer doBatchDeleteAction(DataParam param) {
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
        	String id = idArray[i];
        	String appId = param.get("appId");
        	DataParam idParam = new DataParam();
        	idParam.put("MF_ID", id);
        	idParam.put("APP_ID", appId);
        	getService().deletRecord(idParam);
    	}
		return prepareDisplay(param);
    	
   	}
    
    public  ViewRenderer doConfirmMfStateAction(DataParam param) {
    	String responseText = FAIL;
    	String resource = param.get("resource");
    	int count = 0;
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
        	String id = idArray[i];
        	DataParam idParam = new DataParam();
        	String appId = param.get("appId");
        	idParam.put("MF_ID", id);
        	idParam.put("APP_ID", appId);
        	DataRow record = getService().getRecord(idParam);
        	String appName = record.stringValue("APP_NAME");
			MessageFlowManager messageFlowManager = MessageFlowManager.instance(appName);
			String messageFlowId = record.stringValue("MF_ID");
			if(STOPMF.equals(resource)){
				if (!messageFlowManager.isActive(messageFlowId)){
					count = count + 1;
				}
			}else if(STARTMF.equals(resource) || DODELETEREQ.equals(resource)){
				if (messageFlowManager.isActive(messageFlowId)){
					count = count + 1;
				}
			}
    	}
    	if(count == 0){
    		responseText = resource;
    	}
		return new AjaxRenderer(responseText);
   	}

    protected MfProfileManage getService() {
        return (MfProfileManage) this.lookupService(this.getServiceId());
    }
}
