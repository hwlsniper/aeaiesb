package com.agileai.esb.smc.controller.ws8mf;

import com.agileai.domain.DataParam;
import com.agileai.esb.component.manager.MessageFlowManager;
import com.agileai.esb.smc.bizmoduler.ws8mf.MfProfileManage;
import com.agileai.esb.smc.service.SoakerManage;
import com.agileai.esb.smc.util.SMCHelper;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class MfProfileManageEditHandler
        extends StandardEditHandler {
    public MfProfileManageEditHandler() {
        super();
        this.listHandlerClass = MfProfileManageListHandler.class;
        this.serviceId = buildServiceId(MfProfileManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("SHARELOG",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("SHARELOG",
                                                                               "N")));
        setAttribute("DEPLOYED",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("DEPLOYED",
                                                                               "N")));
        setAttribute("START_WITH_SERVER",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("START_WITH_SERVER",
                                                                               "N")));
        setAttribute("fromNode",param.get("fromNode"));
        
        
        String mfId = getAttributeValue("MF_ID");
        String appName = getAttributeValue("APP_NAME");
    	boolean isActive = MessageFlowManager.instance(appName).isActive(mfId);
    	if (isActive){
    		this.setAttribute("isActive", "已启动");
    		this.setAttribute("operaButton", "停止");
    		this.setAttribute("operaHandler", "stopMF()");
    	}else{
    		this.setAttribute("isActive", "已停止");
    		this.setAttribute("operaButton", "启动");
    		this.setAttribute("operaHandler", "startMF()");
    	}
    	String groupId = param.get("groupId");
    	this.setAttribute("groupId", groupId);
    	
    	String wsId = param.get("wsId");
    	this.setAttribute("wsId", wsId);
    }

    @PageAction
    public ViewRenderer startMF(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	String messageFlowName = param.get("MF_NAME");
        	soakerManage.startMessageFlowProfile(appName, messageFlowName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }
    
    @PageAction
    public ViewRenderer stopMF(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	String messageFlowName = param.get("MF_NAME");
        	soakerManage.stopMessageFlowProfile(appName, messageFlowName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }
    
    public ViewRenderer doViewDetailAction(DataParam param){
		return prepareDisplay(param);
	}
    
    protected MfProfileManage getService() {
        return (MfProfileManage) this.lookupService(this.getServiceId());
    }
}
