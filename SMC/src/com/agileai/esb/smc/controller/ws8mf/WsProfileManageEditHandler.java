package com.agileai.esb.smc.controller.ws8mf;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.esb.component.manager.ServiceConsumerManager;
import com.agileai.esb.component.manager.ServiceProducerManager;
import com.agileai.esb.component.manager.WebServiceManager;
import com.agileai.esb.smc.bizmoduler.ws8mf.WsProfileManage;
import com.agileai.esb.smc.service.SoakerManage;
import com.agileai.esb.smc.util.SMCHelper;

public class WsProfileManageEditHandler
        extends StandardEditHandler {
    public WsProfileManageEditHandler() {
        super();
        this.listHandlerClass = WsProfileManageListHandler.class;
        this.serviceId = buildServiceId(WsProfileManage.class);
    }

	protected void processPageAttributes(DataParam param) {
        setAttribute("DEPLOYED",
                     FormSelectFactory.create("BOOL_DEFINE")
                                      .addSelectedValue(getOperaAttributeValue("DEPLOYED",
                                                                               "N")));
        setAttribute("fromNode",param.get("fromNode"));
        String appName = this.getAttributeValue("APP_NAME");
		String serviceName = this.getAttributeValue("WS_NAME");
		List<DataRow> producerRecords = ServiceProducerManager.instance(appName).getProduceAppRecords(serviceName);
		List<DataRow> consumerRecords = ServiceConsumerManager.instance(appName).getConsumeAppRecords(serviceName);
		
		String procuderName = this.buildNames(producerRecords);
		String consumerName = this.buildNames(consumerRecords);
		this.setAttribute("PRODUCER_NAME", procuderName);
		this.setAttribute("CONSUMER_NAME", consumerName);
        
        
        String wsId = getAttributeValue("WS_ID");
        WebServiceManager webServiceManager = WebServiceManager.instance(appName);
    	boolean isActive = webServiceManager.isActive(wsId);
    	if (isActive){
    		this.setAttribute("isActive", "已启动");
    		this.setAttribute("operaButton", "停止");
    		this.setAttribute("operaHandler", "stopWS()");
    	}else{
    		this.setAttribute("isActive", "已停止");
    		this.setAttribute("operaButton", "启动");
    		this.setAttribute("operaHandler", "startWS()");
    	}
    }
    
    private String buildNames(List<DataRow> producerRecords){
    	String result = "";
    	StringBuffer sb = new StringBuffer();
    	for (int i=0;i < producerRecords.size();i++){
    		DataRow row = producerRecords.get(i);
    		String sysInfoCode = row.getString("SYSINFO_CODE");
    		sb.append(",").append(sysInfoCode);
    	}
    	if (sb.length() > 1){
    		result = sb.substring(1);
    	}
    	return result;
    }
    
    @PageAction
    public ViewRenderer startWS(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	String webServiceName = param.get("WS_NAME");
        	soakerManage.startWebServiceProfile(appName, webServiceName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }
    
    @PageAction
    public ViewRenderer stopWS(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	String webServiceName = param.get("WS_NAME");
        	soakerManage.stopWebServiceProfile(appName, webServiceName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }

    protected WsProfileManage getService() {
        return (WsProfileManage) this.lookupService(this.getServiceId());
    }
}
