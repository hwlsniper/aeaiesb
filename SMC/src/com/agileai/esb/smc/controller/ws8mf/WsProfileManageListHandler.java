package com.agileai.esb.smc.controller.ws8mf;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.component.manager.WebServiceManager;
import com.agileai.esb.smc.bizmoduler.ws8mf.WsProfileManage;
import com.agileai.esb.smc.service.SoakerManage;
import com.agileai.esb.smc.util.SMCHelper;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class WsProfileManageListHandler
        extends StandardListHandler {
	public static String ENABLE = "enable";
	public static String STOP = "stop";
	public static String STARTWS = "startWS";
	public static String STOPWS = "stopWS";
	public static String DODELETEREQ = "doDeleteReq";
    public WsProfileManageListHandler() {
        super();
        this.editHandlerClazz = WsProfileManageEditHandler.class;
        this.serviceId = buildServiceId(WsProfileManage.class);
    }
	public ViewRenderer doDeleteAction(DataParam param){
		SoakerManage soakerManage = SMCHelper.getSoakerManage();
		DataRow row = getService().getRecord(param);
		String appId = row.stringValue("APP_ID");
		String webServiceId = row.stringValue("WS_ID");
		String appName = row.stringValue("APP_NAME");
		String webServiceName = row.stringValue("WS_NAME");
		soakerManage.undeployWebServiceProfile(appName, webServiceName, webServiceId);
		soakerManage.deleteWebServiceProfile(appId,webServiceId);
		storeParam(param);
		this.setAttribute("refreshTree","Y");
		return prepareDisplay(param);
	}
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		String wsState = param.get("wsState");
		List<DataRow> rsList = getService().findRecords(param);
		List<DataRow> records = new ArrayList<DataRow>();
		for (int i=0;i < rsList.size();i++){
			DataRow row = rsList.get(i);
			String webServiceId = row.stringValue("WS_ID");
			String appName = row.stringValue("APP_NAME");
			WebServiceManager webServiceManager = WebServiceManager.instance(appName);
			if (webServiceManager.isActive(webServiceId)){
				row.put("isActiveCode","true");
				row.put("isActive","已启动");
				if(ENABLE.equals(wsState)){
					records.add(row);
				}
			}else{
				row.put("isActiveCode","false");
				row.put("isActive","已停止");
				if(STOP.equals(wsState)){
					records.add(row);
				}
			}
			if(StringUtil.isNullOrEmpty(wsState)){
				records.add(row);
			}
		}
		this.setRsList(records);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
    @PageAction
    public ViewRenderer startWS(DataParam param){
    	ViewRenderer result = null;
    	
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
    		String id = idArray[i];
        	DataParam idParam = new DataParam();
        	String appId = param.get("appId");
        	idParam.put("WS_ID", id);
        	idParam.put("APP_ID", appId);
        	DataRow record = getService().getRecord(idParam);
        	String appName = record.stringValue("APP_NAME");
        	String webServiceName = record.stringValue("WS_NAME");
    		try {
    			SoakerManage soakerManage = SMCHelper.getSoakerManage();
    			soakerManage.startWebServiceProfile(appName, webServiceName);
            	result = new AjaxRenderer(SUCCESS);
    		} catch (Exception e) {
    			result = new AjaxRenderer(FAIL);
    		}
    	}
    	return result;
    }
    
    @PageAction
    public ViewRenderer stopWS(DataParam param){
    	ViewRenderer result = null;
    	
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
    		String id = idArray[i];
        	DataParam idParam = new DataParam();
        	String appId = param.get("appId");
        	idParam.put("WS_ID", id);
        	idParam.put("APP_ID", appId);
        	DataRow record = getService().getRecord(idParam);
        	String appName = record.stringValue("APP_NAME");
        	String webServiceName = record.stringValue("WS_NAME");
    		try {
    			SoakerManage soakerManage = SMCHelper.getSoakerManage();
            	soakerManage.stopWebServiceProfile(appName, webServiceName);
            	result = new AjaxRenderer(SUCCESS);
    		} catch (Exception e) {
    			result = new AjaxRenderer(FAIL);
    		}
    	}
    	return result;
    }
	
	
    protected void processPageAttributes(DataParam param) {
        initMappingItem("DEPLOYED",FormSelectFactory.create("BOOL_DEFINE").getContent());
        
        setAttribute("wsState",
                FormSelectFactory.create("MfState").addSelectedValue(param.get("wsState")));
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "groupId", "");
        initParamItem(param, "appId", "");
    }
    
    public  ViewRenderer doBatchDeleteAction(DataParam param) {
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
        	String id = idArray[i];
        	String appId = param.get("appId");
        	DataParam idParam = new DataParam();
        	idParam.put("WS_ID", id);
        	idParam.put("APP_ID", appId);
        	getService().deletRecord(idParam);
    	}
		return prepareDisplay(param);
    	
   	}
    
    public  ViewRenderer doConfirmWsStateAction(DataParam param) {
    	String responseText = FAIL;
    	String resource = param.get("resource");
    	int count = 0;
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
        	String id = idArray[i];
        	DataParam idParam = new DataParam();
        	String appId = param.get("appId");
        	idParam.put("WS_ID", id);
        	idParam.put("APP_ID", appId);
        	DataRow record = getService().getRecord(idParam);
        	String appName = record.stringValue("APP_NAME");
        	WebServiceManager webServiceManager = WebServiceManager.instance(appName);
			String messageFlowId = record.stringValue("WS_ID");
			if(STOPWS.equals(resource)){
				if (!webServiceManager.isActive(messageFlowId)){
					count = count + 1;
				}
			}else if(STARTWS.equals(resource) || DODELETEREQ.equals(resource)){
				if (webServiceManager.isActive(messageFlowId)){
					count = count + 1;
				}
			}
    	}
    	if(count == 0){
    		responseText = resource;
    	}
		return new AjaxRenderer(responseText);
   	}

    protected WsProfileManage getService() {
        return (WsProfileManage) this.lookupService(this.getServiceId());
    }
}
