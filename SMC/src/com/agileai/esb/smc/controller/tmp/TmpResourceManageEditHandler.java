package com.agileai.esb.smc.controller.tmp;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.esb.smc.bizmoduler.tmp.TmpResourceManage;

public class TmpResourceManageEditHandler extends StandardEditHandler {
	
	public TmpResourceManageEditHandler() {
		this.listHandlerClass = TmpResourceManageListHandler.class;
		this.serviceId = buildServiceId(TmpResourceManage.class);
	}

	protected void processPageAttributes(DataParam param) {
		setAttribute(
				"TEMPT_GRP",
				FormSelectFactory.create("TEMPT_GRP").addSelectedValue(
						getOperaAttributeValue("TEMPT_GRP", "default")));
	}

	protected TmpResourceManage getService() {
		return ((TmpResourceManage) lookupService(getServiceId()));
	}
}