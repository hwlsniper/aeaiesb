package com.agileai.esb.smc.controller;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.smc.bizmoduler.sysres.SkAppManage;
import com.agileai.esb.smc.bizmoduler.sysres.SysUserManage;
import com.agileai.esb.smc.domain.Function;
import com.agileai.esb.smc.domain.User;
import com.agileai.hotweb.common.Constants;
import com.agileai.hotweb.common.Constants.FrameHandlers;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.CryptionUtil;
import com.agileai.util.MapUtil;

public class LoginHandler  extends BaseHandler{
	public LoginHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param) {
		return new LocalRenderer(getPage());
	}
	
	public ViewRenderer doLoginAction(DataParam param){
		ViewRenderer result = null;
		String userId = param.get("userId");
		String userPwd = param.get("userPwd");
		SysUserManage userManage = this.lookupService(SysUserManage.class);
		DataParam queryParam = new DataParam();
		queryParam.put("USER_CODE",userId);
		DataRow recordRow = userManage.getRecordByCode(queryParam);
		
		if (MapUtil.isNullOrEmpty(recordRow)){
			this.setErrorMsg("该用户不存在!");
			result = prepareDisplay(param);
		}
		else if (!recordRow.stringValue("USER_STATE").equals("enable")){
			this.setErrorMsg("该用户处于禁用状态!");
			result = prepareDisplay(param);
		}
		else{
			String encodedPassword = CryptionUtil.md5Hex(userPwd);
			String userPwdTemp = String.valueOf(recordRow.get("USER_PWD"));
			if (userPwdTemp.equals(encodedPassword)){
				initAuthedUser(recordRow);
				result = new RedirectRenderer(getHandlerURL(Constants.FrameHandlers.HomepageHandlerId));
			}
			else{
				this.setErrorMsg("用户ID或者密码不正确!");
				result = prepareDisplay(param);
			}
		}
		return result;
	}
	
	private void initAuthedUser(DataRow recordRow ){
		User user = new User();
		String id = String.valueOf(recordRow.get("USER_ID"));
		String userId = String.valueOf(recordRow.get("USER_CODE"));
		String userName = String.valueOf(recordRow.get("USER_NAME"));
		user.setId(id);
		user.setUserId(userId);
		user.setUserName(userName);
		
		this.initFuncList8Map(user);
		
		String fromIpAddress = request.getLocalAddr();
		Profile profile = new Profile(userId,fromIpAddress,user);
		request.getSession().setAttribute(Profile.PROFILE_KEY, profile);
	}
	
	private void initFuncList8Map(User user) {
		user.getFuncMap().clear();
		user.getFuncList().clear();
		SkAppManage skAppManage = this.lookupService(SkAppManage.class);
		List<DataRow> records = skAppManage.findTreeRecords(user.getUserId());
		for (int i=0;i < records.size();i++){
			DataRow row = records.get(i);
			Function function = new Function();
			String funcId = row.stringValue("FUNC_ID");
			function.setFuncId(funcId);
			function.setFuncName(row.stringValue("FUNC_NAME"));
			function.setFuncPid(row.stringValue("FUNC_PID"));
			function.setFuncUrl(row.stringValue("FUNC_URL"));
			function.setFuncType(row.stringValue("FUNC_TYPE"));
			user.getFuncList().add(function);
			user.getFuncMap().put(funcId, function);
		}
	}	
	
	public ViewRenderer doLogoutAction(DataParam param){
		this.clearSession();
		return new RedirectRenderer(getHandlerURL(FrameHandlers.LoginHandlerId));
	}	
}
