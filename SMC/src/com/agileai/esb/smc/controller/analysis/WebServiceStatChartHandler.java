package com.agileai.esb.smc.controller.analysis;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.smc.bizmoduler.analysis.MessageFlowStatManage;
import com.agileai.esb.smc.bizmoduler.analysis.WebServiceStatManage;
import com.agileai.esb.smc.bizmoduler.sysres.SysUserManage;
import com.agileai.esb.smc.domain.User;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;
import com.agileai.util.XmlUtil;

public class WebServiceStatChartHandler extends BaseHandler{
	
	public WebServiceStatChartHandler(){
		super();
	}

	public ViewRenderer prepareDisplay(DataParam param){
		initParameters(param);
		MessageFlowStatManage service = lookupService(MessageFlowStatManage.class);
		SysUserManage userManage = this.lookupService(SysUserManage.class);
		
		List<DataRow> appRecords = service.findAppRecords();
		User user = (User) this.getUser();
		if(!user.isAdmin()){
			String userId = user.getId();
			HashMap<String,DataRow> userAuthRecords = userManage.findAuthAppMap(userId);
			List<DataRow> results = new ArrayList<DataRow>();
			for (int i = 0; i < appRecords.size(); i++) {
				DataRow appRecord = appRecords.get(i);
				String appName = appRecord.getString("APP_NAME");
				if (userAuthRecords.containsKey(appName)){
					results.add(appRecord);
				}
			}
			appRecords = results;
		}
		FormSelect appNameSelect = new FormSelect();
		appNameSelect.setKeyColumnName("APP_NAME");
		appNameSelect.setValueColumnName("APP_NAME");
		appNameSelect.putValues(appRecords);
		String appSelect = param.get("appName");
		
		if(StringUtil.isNotNullNotEmpty(param.get("appName"))){
			appNameSelect.setSelectedValue(appSelect);
		}
		
		param.put("appName", appNameSelect);
		this.setAttribute("appName",appNameSelect);	
		
		String serviceCode = param.getString("serCode");
		
		if(StringUtil.isNotNullNotEmpty(serviceCode)){
			this.setAttribute("serCode",serviceCode);	
		}	
		this.setAttributes(param);
		return new LocalRenderer(getPage());
	}
	
    protected void initParameters(DataParam param) {
        initParamItem(param, "sdate", DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getBeginOfMonth(new Date())));
        initParamItem(param, "edate", DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, new Date()));
    }    
	@PageAction
	public ViewRenderer query(DataParam param){
		return prepareDisplay(param);
	}
	@PageAction
	public ViewRenderer loadStatData(DataParam param){
		String responseText = null;
		try {
			String sdate = param.get("sdate");
			String edate = param.get("edate");
			String appName = param.get("appName");
			String serviceCode = param.get("serCode");
			int page = param.getInt("page");
			int rows = param.getInt("rows");
			
			String authedAppNames = "";
			User user = (User) this.getUser();
			if(!user.isAdmin()){
				String userId = user.getId();
				SysUserManage userManage = this.lookupService(SysUserManage.class);
				HashMap<String,DataRow> userAuthRecords = userManage.findAuthAppMap(userId);
				Iterator<String> keys = userAuthRecords.keySet().iterator();
				while (keys.hasNext()){
					authedAppNames = authedAppNames + ",'" + keys.next()+"'";
				}
				if (authedAppNames.length() > 1){
					authedAppNames = authedAppNames.substring(1);
				}
			}
			
			WebServiceStatManage service = lookupService(WebServiceStatManage.class);
			List<DataRow> allStatRecordsByPage = service.findStatisticsRecords(sdate, edate,appName,serviceCode,authedAppNames);
			List<DataRow> statRecordsByPage = service.findStatisticsRecords(sdate, edate,appName,page,rows,serviceCode,authedAppNames);
			String strBasicJson = this.buildStatJSON(statRecordsByPage);
			JSONArray basicJsonArray = new JSONArray(strBasicJson);
			JSONObject json = new JSONObject();
	        json.put("rows", basicJsonArray);
	        json.put("total", allStatRecordsByPage.size());
			responseText = json.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
    
	@PageAction
	public ViewRenderer retrieveXml(DataParam param){
		String result = "";
		String infoType = param.get("infoType");
		int topNum = 5;
		String sdate = param.get("sdate");
		String edate = param.get("edate");
		String appName = param.get("appName");
		String serviceCode = param.get("serCode");
		
		String authedAppNames = "";
		User user = (User) this.getUser();
		if(!user.isAdmin()){
			String userId = user.getId();
			SysUserManage userManage = this.lookupService(SysUserManage.class);
			HashMap<String,DataRow> userAuthRecords = userManage.findAuthAppMap(userId);
			Iterator<String> keys = userAuthRecords.keySet().iterator();
			while (keys.hasNext()){
				authedAppNames = authedAppNames + ",'" + keys.next()+"'";
			}
			if (authedAppNames.length() > 1){
				authedAppNames = authedAppNames.substring(1);
			}
		}
		
		WebServiceStatManage service = lookupService(WebServiceStatManage.class);
		if(infoType.equals("ActiveWsStat")){
			List<DataRow> activeMfStatRecords = service.findTopActiveRecords(sdate, edate, topNum,appName,serviceCode,authedAppNames);
			result = this.buildActiveStatXml(activeMfStatRecords);
		}
		else if (infoType.equals("SlowlyWsStat")){
			List<DataRow> slowlyMfStatRecords = service.findTopSlowlyRecords(sdate, edate, topNum,appName,serviceCode,authedAppNames);
			result = this.buildSlowlyStatXml(slowlyMfStatRecords);
		}
		return new AjaxRenderer(result);
	}

	public String buildStatJSON(List<DataRow> statRecords){
		JSONArray jsonArray = new JSONArray();
		try {
	    	for (int i=0;i < statRecords.size();i++){
	    		DataRow row = statRecords.get(i);
	    		JSONObject jsonObject = new JSONObject();
	    		jsonObject.put("appName",row.stringValue("APP_NAME"));
	    		jsonObject.put("webServeiceId",row.stringValue("WS_ID"));
	    		jsonObject.put("webServeiceName",row.stringValue("WS_NAME"));
	    		jsonObject.put("webServeiceAlias",row.stringValue("WS_ALIAS"));
	    		jsonObject.put("webServeiceOperation",row.stringValue("WS_OPERNAME"));
	    		jsonObject.put("webServeiceType",row.stringValue("WS_TYPE"));
	    		jsonObject.put("totalCount",row.stringValue("WS_RUN_COUNT"));
	    		jsonObject.put("successCount",row.stringValue("WS_SUCCESS_COUNT"));
	    		jsonObject.put("failureCount",row.stringValue("WS_ERROR_COUNT"));
	    		jsonObject.put("maxRuntime",row.stringValue("WS_MAX_RUNTIME"));
	    		jsonObject.put("minRuntime",row.stringValue("WS_MIN_RUNTIME"));
	    		long totolRunTime = Long.parseLong(row.stringValue("WS_TOTAL_RUNTIME"));
	    		long totolRunCount = Long.parseLong(row.stringValue("WS_RUN_COUNT"));
	    		jsonObject.put("averageRuntime",totolRunTime/totolRunCount);
	    		jsonArray.put(jsonObject);
	    	}
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return jsonArray.toString();
	}
	
	public String buildSlowlyStatXml(List<DataRow> slowlyMfStatRecords){
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart"); 

		charElement.addAttribute("caption","平均最慢服务方法统计");
		charElement.addAttribute("yAxisName","运行时间");
		charElement.addAttribute("useRoundEdges","1");
//		charElement.addAttribute("bgColor","FFFFFF,FFFFFF");
		charElement.addAttribute("showBorder","0");
		charElement.addAttribute("legendBorderAlpha","0");
		for (int i=0;i < slowlyMfStatRecords.size();i++){
			DataRow row = slowlyMfStatRecords.get(i);
			Element element = charElement.addElement("set");
			String appName = row.stringValue("APP_NAME");
			element.addAttribute("label",appName+"."+row.stringValue("WS_ALIAS"));
			element.addAttribute("value",row.stringValue("WS_AVERAGE_TIME"));
		}
		return document.asXML().toString();
	}
	
	public String buildActiveStatXml(List<DataRow> activeMfStatRecords){
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("chart"); 
		
		charElement.addAttribute("caption","最活跃服务方法统计");
		charElement.addAttribute("yAxisName","运行次数");
		charElement.addAttribute("useRoundEdges","1");
		charElement.addAttribute("showLabels","1");
//		charElement.addAttribute("bgColor","FFFFFF,FFFFFF");
		charElement.addAttribute("showvalues","0");
		charElement.addAttribute("decimals","0");
		charElement.addAttribute("legendBorderAlpha","0");
		
		Element categoriesElement = charElement.addElement("categories");
		
		Element successElement = charElement.addElement("dataset");
		successElement.addAttribute("seriesName", "成功次数");
		successElement.addAttribute("color", "F6BD0F");
		successElement.addAttribute("showValues", "0");
		
		Element failureElement = charElement.addElement("dataset");
		failureElement.addAttribute("seriesName", "失败次数");
		failureElement.addAttribute("color", "8BBA00");
		failureElement.addAttribute("showValues", "0");
		
		for (int i=0;i < activeMfStatRecords.size();i++){
			DataRow row = activeMfStatRecords.get(i);
			String appName = row.stringValue("APP_NAME");
			Element element = categoriesElement.addElement("category");
			element.addAttribute("label",appName+"."+row.stringValue("WS_ALIAS"));
		}
		
		
		for (int i=0;i < activeMfStatRecords.size();i++){
			DataRow row = activeMfStatRecords.get(i);
			Element element = successElement.addElement("set");
			element.addAttribute("value",row.stringValue("WS_SUCCESS_COUNT"));
		}
		
		for (int i=0;i < activeMfStatRecords.size();i++){
			DataRow row = activeMfStatRecords.get(i);
			Element element = failureElement.addElement("set");
			element.addAttribute("value",row.stringValue("WS_ERROR_COUNT"));
		}		
		
		return document.asXML().toString();
	}	
}