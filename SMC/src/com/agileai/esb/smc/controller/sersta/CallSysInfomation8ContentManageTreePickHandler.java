package com.agileai.esb.smc.controller.sersta;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.smc.bizmoduler.sersta.CallSysInfomation8ContentManage;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;

public class CallSysInfomation8ContentManageTreePickHandler
        extends TreeSelectHandler {
    public CallSysInfomation8ContentManageTreePickHandler() {
        super();
        this.serviceId = buildServiceId(CallSysInfomation8ContentManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "SYSINFO_ID",
                                                  "SYSINFO_NAME", "SYSINFO_PID");

        String excludeId = param.get("SYSINFO_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected CallSysInfomation8ContentManage getService() {
        return (CallSysInfomation8ContentManage) this.lookupService(this.getServiceId());
    }
}
