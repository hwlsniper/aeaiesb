package com.agileai.esb.smc.controller.sersta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.component.manager.ServiceConsumerManager;
import com.agileai.esb.smc.bizmoduler.sersta.CallSysInfomation8ContentManage;
import com.agileai.esb.smc.controller.analysis.WebServiceStatChartHandler;
import com.agileai.esb.smc.controller.monitor.WSRuntimeStatListHandler;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.controller.core.TreeAndContentManageListHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class CallSysInfomation8ContentListHandler
        extends TreeAndContentManageListHandler {
	
    public CallSysInfomation8ContentListHandler() {
        super();
        this.serviceId = buildServiceId(CallSysInfomation8ContentManage.class);
        this.rootColumnId = "00000000-0000-0000-00000000000000000";
        this.defaultTabId = "ServiceInfomation";
        this.columnIdField = "SYSINFO_ID";
        this.columnNameField = "SYSINFO_NAME";
        this.columnParentIdField = "SYSINFO_PID";
        this.columnSortField = "SYSINFO_SORT";
    }

    protected void processPageAttributes(DataParam param) {
        String tabId = param.get(TreeAndContentManage.TAB_ID, this.defaultTabId);

        if ("ServiceInfomation".equals(tabId)) {
        }
        
        CallSysInfomation8ContentManage callSysInfomation8ContentManage = this.lookupService(CallSysInfomation8ContentManage.class);
    	List<DataRow> appList = callSysInfomation8ContentManage.queryAppProjectRecords();
		FormSelect formSelect = new FormSelect();
		formSelect.setKeyColumnName("APP_ID");
		formSelect.setValueColumnName("APP_NAME");
		formSelect.putValues(appList);
		setAttribute("appName",formSelect.addSelectedValue(param.get("appName")));
		setAttribute("serviceType",
                FormSelectFactory.create("SERVICE_TYPE")
                                 .addSelectedValue(getAttributeValue("serviceType",
                                                                          "")));
    }

    protected void initParameters(DataParam param) {
        String tabId = param.get(TreeAndContentManage.TAB_ID, this.defaultTabId);

        if ("ServiceInfomation".equals(tabId)) {
            initParamItem(param, "serviceCode", "");
            initParamItem(param, "serviceName", "");
            initParamItem(param, "serviceType", "");
        }
    }

    public ViewRenderer doViewWSRuntimeStatListAction(DataParam param){
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(WSRuntimeStatListHandler.class));
	}	
    
    public ViewRenderer doViewWebServiceStatChartAction(DataParam param){
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(WebServiceStatChartHandler.class));
	}
    
    protected TreeBuilder provideTreeBuilder(DataParam param) {
        CallSysInfomation8ContentManage service = this.getService();
        List<DataRow> menuRecords = service.findTreeRecords(new DataParam());
        TreeBuilder treeBuilder = new TreeBuilder(menuRecords,
                                                  this.columnIdField,
                                                  this.columnNameField,
                                                  this.columnParentIdField);

        return treeBuilder;
    }
    
	public ViewRenderer doDeleteAction(DataParam param){
		TreeAndContentManage service = this.getService();
		String tabId = param.get(TreeAndContentManage.TAB_ID);
		String columnId = param.get("curColumnId");
		Map<String,String> tabIdAndColFieldMapping = service.getTabIdAndColFieldMapping();
		String colField = tabIdAndColFieldMapping.get(tabId);
		param.put(colField,columnId);
		
		String serviceId = param.get("SERVICE_ID");
		DataParam queryParam = new DataParam("SERVICE_ID",serviceId);
		DataRow dataRow = getService().getContentRecord(this.defaultTabId, queryParam);
		String appName = dataRow.getString("SERVICE_PROJECT");
		String serviceName = dataRow.getString("SERVICE_CODE");
		ServiceConsumerManager.instance(appName).refresh(serviceName);
		
		service.deletContentRecord(tabId,param);	
		return prepareDisplay(param);
	}

    protected List<String> getTabList() {
        List<String> result = new ArrayList<String>();
        result.add("ServiceInfomation");

        return result;
    }

    protected CallSysInfomation8ContentManage getService() {
        return (CallSysInfomation8ContentManage) this.lookupService(this.getServiceId());
    }
}
