package com.agileai.esb.smc.controller.sersta;

import com.agileai.domain.DataParam;
import com.agileai.esb.smc.bizmoduler.sersta.SysInfomation8ContentManage;
import com.agileai.hotweb.controller.core.TreeAndContentColumnEditHandler;

public class SysInfomationEditHandler
        extends TreeAndContentColumnEditHandler {
    public SysInfomationEditHandler() {
        super();
        this.serviceId = buildServiceId(SysInfomation8ContentManage.class);
        this.columnIdField = "SYSINFO_ID";
        this.columnParentIdField = "SYSINFO_PID";
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected SysInfomation8ContentManage getService() {
        return (SysInfomation8ContentManage) this.lookupService(this.getServiceId());
    }
}
