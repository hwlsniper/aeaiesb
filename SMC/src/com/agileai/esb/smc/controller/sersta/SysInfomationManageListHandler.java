package com.agileai.esb.smc.controller.sersta;

import com.agileai.domain.DataParam;
import com.agileai.esb.component.manager.SysInfomationManager;
import com.agileai.esb.smc.bizmoduler.sersta.SysInfomationManage;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.ViewRenderer;

public class SysInfomationManageListHandler
        extends StandardListHandler {
    public SysInfomationManageListHandler() {
        super();
        this.editHandlerClazz = SysInfomationManageEditHandler.class;
        this.serviceId = buildServiceId(SysInfomationManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    	setAttribute("sysinfoType",
                FormSelectFactory.create("SYS_TYPE")
                                 .addSelectedValue(param.get("sysinfoType")));    	
    	 initMappingItem("SYSINFO_TYPE",
                 FormSelectFactory.create("SYS_TYPE").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "sysinfoCode", "");
        initParamItem(param, "sysinfoName", "");
        initParamItem(param, "sysinfoType", "");
    }

    public ViewRenderer doDeleteAction(DataParam param){
    	String sysInfoId = param.get("SYSINFO_ID");
    	SysInfomationManager.instance().refresh(sysInfoId);
    	return super.doDeleteAction(param);
    }
    
    protected SysInfomationManage getService() {
        return (SysInfomationManage) this.lookupService(this.getServiceId());
    }
}
