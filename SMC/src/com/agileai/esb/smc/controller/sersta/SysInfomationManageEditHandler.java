package com.agileai.esb.smc.controller.sersta;

import com.agileai.domain.DataParam;
import com.agileai.esb.component.manager.SysInfomationManager;
import com.agileai.esb.smc.bizmoduler.sersta.SysInfomationManage;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class SysInfomationManageEditHandler
        extends StandardEditHandler {
    public SysInfomationManageEditHandler() {
        super();
        this.listHandlerClass = SysInfomationManageListHandler.class;
        this.serviceId = buildServiceId(SysInfomationManage.class);
    }
    
	public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateRecord(param);	
		}
		String sysInfoId = param.get("SYSINFO_ID");
		SysInfomationManager.instance().refresh(sysInfoId);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
    
    protected void processPageAttributes(DataParam param) {
    	this.setAttribute("SYSINFO_PID", "00000000-0000-0000-00000000000000000");
    	
    	setAttribute("SYSINFO_TYPE",
                FormSelectFactory.create("SYS_TYPE")
                                 .addSelectedValue(getAttributeValue("SYSINFO_TYPE",
                                                                          "")));
    }

    protected SysInfomationManage getService() {
        return (SysInfomationManage) this.lookupService(this.getServiceId());
    }
}
