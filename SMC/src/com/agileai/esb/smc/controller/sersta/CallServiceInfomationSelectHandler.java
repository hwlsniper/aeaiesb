package com.agileai.esb.smc.controller.sersta;

import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.component.manager.ServiceConsumerManager;
import com.agileai.esb.smc.bizmoduler.sersta.CallServiceInfomationSelect;
import com.agileai.esb.smc.bizmoduler.sersta.CallSysInfomation8ContentManage;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class CallServiceInfomationSelectHandler
        extends PickFillModelHandler {
    public CallServiceInfomationSelectHandler() {
        super();
        this.serviceId = buildServiceId(CallServiceInfomationSelect.class);
    }
    
    protected void processPageAttributes(DataParam param) {
    	CallSysInfomation8ContentManage callSysInfomation8ContentManage = this.lookupService(CallSysInfomation8ContentManage.class);
    	List<DataRow> appList = callSysInfomation8ContentManage.queryAppProjectRecords();
		FormSelect formSelect = new FormSelect();
		formSelect.setKeyColumnName("APP_ID");
		formSelect.setValueColumnName("APP_NAME");
		formSelect.putValues(appList);
		setAttribute("appName",formSelect.addSelectedValue(param.get("appName")));
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "serviceName", "");
        initParamItem(param, "serviceCode", "");
        initParamItem(param, "appName", "");
    }

    protected CallServiceInfomationSelect getService() {
        return (CallServiceInfomationSelect) this.lookupService(this.getServiceId());
    }
    
    @PageAction
    public ViewRenderer addService(DataParam param){
    	String responseText = FAIL;
    	try {
    		String ids = param.getString("ids");
        	String sysinfoId = param.getString("SYSINFO_ID");
        	CallSysInfomation8ContentManage callSysInfomation8ContentManage = this.lookupService(CallSysInfomation8ContentManage.class);
        	List<DataRow> records = callSysInfomation8ContentManage.findServiceRecordsBySysinfoId(param);
        	int count = records.size();
        	if(!"".equals(ids)){
        		String[] idArray = ids.split(",");
        		
        		for (int i = 0; i < idArray.length; i++) {
        			String id = idArray[i];
        			
        			DataParam queryParam = new DataParam("SERVICE_ID",id);
            		DataRow dataRow = callSysInfomation8ContentManage.getContentRecord("ServiceInfomation", queryParam);
        			String appName = dataRow.getString("SERVICE_PROJECT");
        			String serviceName = dataRow.getString("SERVICE_CODE");
        			ServiceConsumerManager.instance(appName).refresh(serviceName);
            		
        			String rsrId = KeyGenerator.instance().genKey();  
        			DataParam dataParam = new DataParam("CSR_ID",rsrId,"SYSINFO_ID",sysinfoId,"SERVICE_ID",id,"CSR_SORT",count+i+1);
        			callSysInfomation8ContentManage.addService(dataParam);
    			}
        	}
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
}
