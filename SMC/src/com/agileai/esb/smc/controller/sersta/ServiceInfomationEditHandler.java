package com.agileai.esb.smc.controller.sersta;

import com.agileai.domain.DataParam;
import com.agileai.esb.component.manager.WebServiceManager;
import com.agileai.esb.smc.bizmoduler.sersta.SysInfomation8ContentManage;
import com.agileai.esb.wsmodel.WSDescription;
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;

public class ServiceInfomationEditHandler
        extends TreeAndContentManageEditHandler {
    public ServiceInfomationEditHandler() {
        super();
        this.serviceId = buildServiceId(SysInfomation8ContentManage.class);
        this.tabId = "ServiceInfomation";
        this.columnIdField = "SYSINFO_ID";
        this.contentIdField = "SERVICE_ID";
    }

    protected void processPageAttributes(DataParam param) {
    	String serviceType = this.getAttributeValue("SERVICE_TYPE");
    	if ("Web Service".equals(serviceType)){
    		String serviceCode = this.getAttributeValue("SERVICE_CODE");
    		String appName = this.getAttributeValue("SERVICE_PROJECT");
    		WebServiceManager webServiceManager = WebServiceManager.instance(appName);
    		WSDescription wsDescription = webServiceManager.getWsmodels().get(serviceCode);
    		if (wsDescription != null){
        		boolean enableBizLog = wsDescription.isEnableBizLog();
        		if (enableBizLog){
        			this.setAttribute("ENABLE_BIZLOG", "Y");
        			this.setAttribute("BZILOG_CONFIG",wsDescription.getBizLogFlowName());
        		}else{
        			this.setAttribute("ENABLE_BIZLOG", "N");	
        		}
    		}else{
    			this.setErrorMsg("服务没有部署，请检查！");
    		}
    	}    	
    	setAttribute("ENABLE_BIZLOG",
                FormSelectFactory.create("BOOL_DEFINE")
                                 .addSelectedValue(getOperaAttributeValue("ENABLE_BIZLOG",
                                                                          "N")));
    }

    protected SysInfomation8ContentManage getService() {
        return (SysInfomation8ContentManage) this.lookupService(this.getServiceId());
    }
}
