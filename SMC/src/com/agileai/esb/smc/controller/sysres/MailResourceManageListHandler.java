package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.esb.component.manager.GenResourceManager;
import com.agileai.esb.core.SoakerContants;
import com.agileai.esb.smc.bizmoduler.sysres.MailResourceManage;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class MailResourceManageListHandler
        extends StandardListHandler {
    public MailResourceManageListHandler() {
        super();
        this.editHandlerClazz = MailResourceManageEditHandler.class;
        this.serviceId = buildServiceId(MailResourceManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    }
    
    @PageAction
    public ViewRenderer reloadResources(DataParam param){
    	String responseText = FAIL;
    	try {
    		GenResourceManager.instance(SoakerContants.GenResTypes.Mail).refreshAll();
    		responseText = SUCCESS;
		} catch (Exception e) {
			responseText = FAIL;
		}
    	return new AjaxRenderer(responseText);
    } 

    protected MailResourceManage getService() {
        return (MailResourceManage) this.lookupService(this.getServiceId());
    }
}
