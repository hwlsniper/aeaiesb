package com.agileai.esb.smc.controller.sysres;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.component.manager.WebServiceProxyManager;
import com.agileai.esb.smc.bizmoduler.sysres.WsProxyManage;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class WsProxyManageListHandler
        extends StandardListHandler {
    public WsProxyManageListHandler() {
        super();
        this.editHandlerClazz = WsProxyManageEditHandler.class;
        this.serviceId = buildServiceId(WsProxyManage.class);
    }
    
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		String appId = param.get("APP_ID"); 
		this.setAttribute("APP_ID", appId);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
    	initMappingItem("WS_STATE",
                FormSelectFactory.create("BOOL_DEFINE").getContent());
        initMappingItem("ENABLE_AUTH",
                        FormSelectFactory.create("ENABLE_AUTH").getContent());
        initMappingItem("ENABLE_BIZLOG",
                        FormSelectFactory.create("BOOL_DEFINE").getContent());
    }

    protected void initParameters(DataParam param) {
    }
    
    @PageAction
    public ViewRenderer reloadAll(DataParam param){
    	String responseText = FAIL;
    	try {
    		String appId = param.get("APP_ID");
    		DataRow record = getService().getAppRecord(appId);
    		String appName = record.getString("APP_NAME");
    		WebServiceProxyManager.instance(appName).refreshProxyModels();
    		responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	
    	return new AjaxRenderer(responseText);
    }

    public  ViewRenderer doBatchDeleteAction(DataParam param) {
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
        	String id = idArray[i];
        	DataParam idParam = new DataParam();
        	idParam.put("WS_ID", id);
        	getService().deletRecord(idParam);
    	}
		return prepareDisplay(param);
    	
   	}
    protected WsProxyManage getService() {
        return (WsProxyManage) this.lookupService(this.getServiceId());
    }
}
