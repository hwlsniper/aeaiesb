package com.agileai.esb.smc.controller.sysres;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.esb.smc.bizmoduler.sysres.SkAppManage;
import com.agileai.esb.smc.bizmoduler.sysres.WsProxyManage;
import com.agileai.esb.smc.bizmoduler.ws8mf.MfProfileManage;
import com.agileai.esb.smc.bizmoduler.ws8mf.WsProfileManage;
import com.agileai.esb.smc.controller.MainWinHandler;
import com.agileai.esb.smc.service.SoakerManage;
import com.agileai.esb.smc.util.SMCHelper;

public class SkAppManageEditHandler
        extends StandardEditHandler {
    public SkAppManageEditHandler() {
        super();
        this.listHandlerClass = SkAppManageListHandler.class;
        this.serviceId = buildServiceId(SkAppManage.class);
    }
    protected void processPageAttributes(DataParam param) {
    	String appName = getAttributeValue("APP_NAME"); 
    	boolean isActive = SMCHelper.isActive(appName);
    	if (isActive){
    		this.setAttribute("isActive", "已启动");
    		this.setAttribute("operaButton", "停止应用");
    		this.setAttribute("operaHandler", "stopApp()");
    	}else{
    		this.setAttribute("isActive", "已停止");
    		this.setAttribute("operaButton", "启动应用");
    		this.setAttribute("operaHandler", "startApp()");
    	}
    }
    @PageAction
    public ViewRenderer deleteApp(DataParam param){
    	SoakerManage soakerManage = SMCHelper.getSoakerManage();
    	String appId = param.get("APP_ID");
    	SkAppManage skAppManage = this.lookupService(SkAppManage.class);
    	DataRow row = skAppManage.getRecord(param);
    	String appName = row.getString("APP_NAME");
    	soakerManage.undeployApplication(appName);
    	skAppManage.deleteTreeGroupRecords(appId);
    	soakerManage.deleteApplication(appId);
    	return new RedirectRenderer(this.getHandlerURL(MainWinHandler.class)+"&refreshTree=Y");
    }
    @PageAction
    public ViewRenderer isExistRelResource(DataParam param){
    	String responseText = "N";
    	String appId = param.get("APP_ID");
    	DataParam queryParam = new DataParam("appId",appId);
    	MfProfileManage mfProfileManage = this.lookupService(MfProfileManage.class);
    	List<DataRow> mfRecords = mfProfileManage.findRecords(queryParam);
    	if (mfRecords != null && mfRecords.size() > 0){
    		responseText = "hasMF";
    		return new AjaxRenderer(responseText);
    	}
    	
    	WsProfileManage wsProfileManage = this.lookupService(WsProfileManage.class);
    	List<DataRow> wsRecords = wsProfileManage.findRecords(queryParam);
    	if (wsRecords != null && wsRecords.size() > 0){
    		responseText = "hasWS";
    		return new AjaxRenderer(responseText);
    	}
    	
    	WsProxyManage wsProxyManage = this.lookupService(WsProxyManage.class);
    	queryParam.put("APP_ID",appId);
    	List<DataRow> wsProRecords = wsProxyManage.findRecords(queryParam);
    	if (wsProxyManage != null && wsProRecords.size() > 0){
    		responseText = "hasWSPRO";
    		return new AjaxRenderer(responseText);
    	}

    	return new AjaxRenderer(responseText);
    }
    
    @PageAction
    public ViewRenderer startApp(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	soakerManage.startApplication(appName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }
    
    @PageAction
    public ViewRenderer stopApp(DataParam param){
    	ViewRenderer result = null;
    	try {
        	SoakerManage soakerManage = SMCHelper.getSoakerManage();
        	String appName = param.get("APP_NAME");
        	soakerManage.stopApplication(appName);
        	result = new AjaxRenderer(SUCCESS);
		} catch (Exception e) {
			result = new AjaxRenderer(FAIL);
		}
    	return result;
    }
    
    public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateRecord(param);	
		}
		return prepareDisplay(param);
	}
    
    protected SkAppManage getService() {
        return (SkAppManage) this.lookupService(this.getServiceId());
    }
}
