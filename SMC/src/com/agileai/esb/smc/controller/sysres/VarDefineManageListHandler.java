package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.component.manager.PropertiesResourceManager;
import com.agileai.esb.smc.bizmoduler.sysres.VarDefineManage;
import com.agileai.hotweb.controller.core.MasterSubListHandler;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class VarDefineManageListHandler
        extends MasterSubListHandler {
    public VarDefineManageListHandler() {
        super();
        this.editHandlerClazz = VarDefineManageEditHandler.class;
        this.serviceId = buildServiceId(VarDefineManage.class);
    }

	public ViewRenderer doDeleteAction(DataParam param){
		storeParam(param);
		
		DataRow row = getService().getMasterRecord(param);
		String varCode = row.stringValue("VAR_CODE");
		PropertiesResourceManager.instance().getPropertiesConfig().remove(varCode);
		
		getService().deleteClusterRecords(param);	
		
		return new RedirectRenderer(getHandlerURL(getClass()));
	}
    
    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    }

    protected VarDefineManage getService() {
        return (VarDefineManage) this.lookupService(this.getServiceId());
    }
}
