package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.esb.smc.bizmoduler.sysres.MqContainerManage;

public class MqContainerManageListHandler
        extends StandardListHandler {
    public MqContainerManageListHandler() {
        super();
        this.editHandlerClazz = MqContainerManageEditHandler.class;
        this.serviceId = buildServiceId(MqContainerManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    	FormSelect brokerIdFormSelect = FormSelectFactory.create("mqresource.findRecords",new DataParam(),"BROKER_ID","BROKER_ALIAS").addBlankValue();
    	setAttribute("brokerId",brokerIdFormSelect.addSelectedValue(param.get("brokerId")));
        setAttribute("objectType",
                     FormSelectFactory.create("MQContainerType")
                                      .addSelectedValue(param.get("objectType")));
        initMappingItem("BROKER_NAME",brokerIdFormSelect.getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "brokerId", "");
        initParamItem(param, "objectType", "Queue");
        initParamItem(param, "objectName", "");
    }

    protected MqContainerManage getService() {
        return (MqContainerManage) this.lookupService(this.getServiceId());
    }
}
