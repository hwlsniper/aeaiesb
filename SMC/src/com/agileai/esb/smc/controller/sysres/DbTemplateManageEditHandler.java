package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.esb.component.manager.DBResourceManager;
import com.agileai.esb.smc.bizmoduler.sysres.DbTemplateManage;

public class DbTemplateManageEditHandler
        extends StandardEditHandler {
    public DbTemplateManageEditHandler() {
        super();
        this.listHandlerClass = DbTemplateManageListHandler.class;
        this.serviceId = buildServiceId(DbTemplateManage.class);
    }
	public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		if (OperaType.CREATE.equals(operateType)){
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateRecord(param);	
		}
		String templateId = param.get("DB_TMP_ID");
		DataRow row = getService().getRecord(new DataParam("DB_TMP_ID",templateId));
		DBResourceManager.instance().refreshDataBase(row);
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
    protected void processPageAttributes(DataParam param) {
        setAttribute("DB_TYPE",
                     FormSelectFactory.create("DataBaseType")
                                      .addSelectedValue(getOperaAttributeValue("DB_TYPE",
                                                                               "Oracle")));
    }

    protected DbTemplateManage getService() {
        return (DbTemplateManage) this.lookupService(this.getServiceId());
    }
}
