package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.esb.smc.bizmoduler.sysres.DbTemplateManage;

public class DbTemplateManageListHandler
        extends StandardListHandler {
    public DbTemplateManageListHandler() {
        super();
        this.editHandlerClazz = DbTemplateManageEditHandler.class;
        this.serviceId = buildServiceId(DbTemplateManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        initMappingItem("DB_TYPE",
                        FormSelectFactory.create("DataBaseType").getContent());
    }

    protected void initParameters(DataParam param) {
    }

    protected DbTemplateManage getService() {
        return (DbTemplateManage) this.lookupService(this.getServiceId());
    }
}
