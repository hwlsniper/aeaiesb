package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.esb.smc.bizmoduler.sysres.MailResourceManage;
import com.agileai.hotweb.controller.core.StandardEditHandler;

public class MailResourceManageEditHandler
        extends StandardEditHandler {
    public MailResourceManageEditHandler() {
        super();
        this.listHandlerClass = MailResourceManageListHandler.class;
        this.serviceId = buildServiceId(MailResourceManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected MailResourceManage getService() {
        return (MailResourceManage) this.lookupService(this.getServiceId());
    }
}
