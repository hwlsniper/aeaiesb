package com.agileai.esb.smc.controller.sysres;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.component.manager.PropertiesResourceManager;
import com.agileai.esb.smc.bizmoduler.sysres.VarDefineManage;
import com.agileai.hotweb.controller.core.MasterSubEditMainHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class VarDefineManageEditHandler
        extends MasterSubEditMainHandler {
    public VarDefineManageEditHandler() {
        super();
        this.listHandlerClass = VarDefineManageListHandler.class;
        this.serviceId = buildServiceId(VarDefineManage.class);
        this.baseTablePK = "VAR_ID";
        this.defaultTabId = "VarDefentry";
    }

    protected void processPageAttributes(DataParam param) {
    	FormSelect encriptionFormSelect = FormSelectFactory.createSwitchFormSelect(); 
    	setAttribute("ENCRIPTION",encriptionFormSelect);
    }

    protected String[] getEntryEditFields(String currentSubTableId) {
        List<String> temp = new ArrayList<String>();

        if ("VarDefentry".equals(currentSubTableId)) {
            temp.add("ENTRY_ID");
            temp.add("ENTRY_CODE");
            temp.add("ENTRY_NAME");
            temp.add("ENCRIPTION");
            temp.add("ENTRY_VALUE");
            temp.add("ENTRY_SORT");
            temp.add("VAR_ID");
        }

        return temp.toArray(new String[] {  });
    }

	public ViewRenderer doSaveMasterRecordAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		String responseText = "fail";
		if (OperaType.CREATE.equals(operateType)){
			getService().createMasterRecord(param);
			responseText = param.get(baseTablePK);
		}
		else if(OperaType.UPDATE.equals(operateType)){
			getService().updateMasterRecord(param);
			saveSubRecords(param);
			responseText = param.get(baseTablePK);
			
			DataRow row = getService().getMasterRecord(param);
			List<DataRow> entries = getService().findSourceSubRecords("VarDefentry", param);
			PropertiesResourceManager.instance().refreshProperties(row, entries);
		}
		return new AjaxRenderer(responseText);
	}    
    
	public ViewRenderer doAddEntryRecordAction(DataParam param){
		String currentSubTableId = param.get("currentSubTableId");
		int currentRecordSize = param.getInt("currentRecordSize");
		List<DataRow> subRecords = new ArrayList<DataRow>();
		String[] entryEditFields = this.getEntryEditFields(currentSubTableId);
		for (int i=0;i < currentRecordSize;i++){
			DataRow row = new DataRow();
			for (int j=0; j < entryEditFields.length;j++){
				String field = entryEditFields[j];
				row.put(field,param.get(field+"_"+i));
			}
			row.put("_state",param.get("state_"+i));
			subRecords.add(row);
		}
		String foreignKey = this.getEntryEditForeignKey(currentSubTableId);
		subRecords.add(new DataRow("_state","insert",foreignKey,param.get(baseTablePK),"ENCRIPTION","N"));
		String subRecordsKey = currentSubTableId + "Records";
		this.setAttribute(subRecordsKey, subRecords);
		return prepareDisplay(param); 
	}
	
	public ViewRenderer doDeleteEntryRecordAction(DataParam param){
		String currentSubTableId = param.get("currentSubTableId");
		int currentRecordIndex = param.getInt("currentRecordIndex");
		String state = param.get("state_"+currentRecordIndex);

		int currentRecordSize = param.getInt("currentRecordSize");
		List<DataRow> subRecords = new ArrayList<DataRow>();
		String[] entryEditFields = this.getEntryEditFields(currentSubTableId);
		for (int i=0;i < currentRecordSize;i++){
			if (i == currentRecordIndex)continue;
			DataRow row = new DataRow();
			for (int j=0; j < entryEditFields.length;j++){
				String field = entryEditFields[j];
				row.put(field,param.get(field+"_"+i));
			}
			row.put("_state",param.get("state_"+i));
			subRecords.add(row);
		}
		String subRecordsKey = currentSubTableId + "Records";
		this.setAttribute(subRecordsKey, subRecords);
		
		if (!state.equals("insert")){
			DataParam deleteParam = new DataParam();
			String pKField = getEntryEditTablePK(currentSubTableId);
			deleteParam.put(pKField,param.get(pKField+"_"+currentRecordIndex));
			getService().deleteSubRecord(currentSubTableId, deleteParam);
		}
		
		
		DataRow row = getService().getMasterRecord(param);
		List<DataRow> entries = getService().findSourceSubRecords("VarDefentry", param);
		PropertiesResourceManager.instance().refreshProperties(row, entries);
		return prepareDisplay(param);
	}
    
    protected String getEntryEditTablePK(String currentSubTableId) {
        HashMap<String, String> primaryKeys = new HashMap<String, String>();
        primaryKeys.put("VarDefentry", "ENTRY_ID");

        return primaryKeys.get(currentSubTableId);
    }

    protected String getEntryEditForeignKey(String currentSubTableId) {
        HashMap<String, String> foreignKeys = new HashMap<String, String>();
        foreignKeys.put("VarDefentry", "VAR_ID");

        return foreignKeys.get(currentSubTableId);
    }

    protected VarDefineManage getService() {
        return (VarDefineManage) this.lookupService(this.getServiceId());
    }
}
