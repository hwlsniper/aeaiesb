package com.agileai.esb.smc.controller.sysres;

import com.agileai.domain.DataParam;
import com.agileai.esb.smc.bizmoduler.sysres.FtpResourceManage;
import com.agileai.hotweb.controller.core.StandardEditHandler;

public class FtpResourceManageEditHandler
        extends StandardEditHandler {
    public FtpResourceManageEditHandler() {
        super();
        this.listHandlerClass = FtpResourceManageListHandler.class;
        this.serviceId = buildServiceId(FtpResourceManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected FtpResourceManage getService() {
        return (FtpResourceManage) this.lookupService(this.getServiceId());
    }
}
