package com.agileai.esb.smc.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "WSFolder")
@XmlAccessorType(XmlAccessType.FIELD)
public class WSFolder {
	private String id = null;
	private String name = null;
	private String applicationId = null;
	private String applicationName = null;
	private String parentId = null;
	
	private List<WSFolder> children = new ArrayList<WSFolder>();
	private List<WSProfile> wsProfileList = new ArrayList<WSProfile>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<WSFolder> getChildren() {
		return children;
	}
	public List<WSProfile> getWsProfileList() {
		return wsProfileList;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentName) {
		this.parentId = parentName;
	}
}
