package com.agileai.esb.smc.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "Application")
@XmlAccessorType(XmlAccessType.FIELD)
public class Application extends GovernPolicy{
	private String id = null;
	private String name = null;
	private String alias = null;
	private String description = null;
	private String mainPkg = null;
	
	private boolean deployed = false;
	private boolean active = false;
	private boolean ceateSample = false;
	
	private List<String> deployedWsIdList = new ArrayList<String>();
	private List<String> deployedMfIdList = new ArrayList<String>();

	private List<String> activeWsIdList = new ArrayList<String>();
	private List<String> activeMfIdList = new ArrayList<String>();
	
	private List<WSFolder> wsFolderList = new ArrayList<WSFolder>();
	private List<MFFolder> mfFolderList = new ArrayList<MFFolder>(); 
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<WSFolder> getWsFolderList() {
		return wsFolderList;
	}
	public List<MFFolder> getMfFolderList() {
		return mfFolderList;
	}
	
	public String getMainPkg() {
		return mainPkg;
	}
	public void setMainPkg(String mainPkg) {
		this.mainPkg = mainPkg;
	}
	public boolean isCeateSample() {
		return ceateSample;
	}
	public void setCeateSample(boolean ceateSample) {
		this.ceateSample = ceateSample;
	}
	public boolean isDeployed() {
		return deployed;
	}
	public void setDeployed(boolean isDeploy) {
		this.deployed = isDeploy;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean isActive) {
		this.active = isActive;
	}
	public List<String> getDeployedWsIdList() {
		return deployedWsIdList;
	}
	public List<String> getDeployedMfIdList() {
		return deployedMfIdList;
	}
	public List<String> getActiveWsIdList() {
		return activeWsIdList;
	}
	public List<String> getActiveMfIdList() {
		return activeMfIdList;
	}
}