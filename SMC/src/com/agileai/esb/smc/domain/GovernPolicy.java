package com.agileai.esb.smc.domain;

public class GovernPolicy {
	protected String createUser = null;
	protected String updateUser = null;
	protected String deployUser = null;
		
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getDeployUser() {
		return deployUser;
	}
	public void setDeployUser(String deployUser) {
		this.deployUser = deployUser;
	}
}
