package com.agileai.esb.smc.domain;

import java.util.ArrayList;
import java.util.List;

import com.agileai.common.KeyValuePair;

public class GenResource {
	private String resType = null;
	private String resCode = null;
	private String resName = null;

	private List<KeyValuePair> properties = new ArrayList<KeyValuePair>();
	
	public String getResType() {
		return resType;
	}

	public void setResType(String resType) {
		this.resType = resType;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public List<KeyValuePair> getProperties() {
		return properties;
	}

	public void setProperties(List<KeyValuePair> properties) {
		this.properties = properties;
	}
}
