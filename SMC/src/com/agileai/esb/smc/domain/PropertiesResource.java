package com.agileai.esb.smc.domain;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "PropertiesResource")
@XmlAccessorType(XmlAccessType.FIELD)
public class PropertiesResource {
	private String code = null;
	private List<String> propertyKeyList = new ArrayList<String>();

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<String> getPropertyKeyList() {
		return propertyKeyList;
	}
}
