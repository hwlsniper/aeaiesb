package com.agileai.esb.smc.common;

import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import com.agileai.esb.component.RuntimeAnalysis;
import com.agileai.esb.component.RuntimeStat;
import com.agileai.esb.smc.bizmoduler.analysis.MessageFlowStatManage;

public class MfStatisticsTask extends TimerTask{
	private MessageFlowStatManage messageFlowStatManage = null;
	
	public MfStatisticsTask(MessageFlowStatManage messageFlowStatManage){
		this.messageFlowStatManage = messageFlowStatManage;
	}
	
	@Override
	public void run() {
		ConcurrentHashMap<String,RuntimeStat> runtimeStats = RuntimeAnalysis.geInstance(true).getTransitRuntimeStatMap();
		messageFlowStatManage.insertStatRecords(runtimeStats);
	}
}
