package com.agileai.esb.smc.common;

import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import com.agileai.esb.smc.bizmoduler.analysis.WebServiceStatManage;
import com.agileai.esb.wsmodel.WSRuntimeAnalysis;
import com.agileai.esb.wsmodel.WSRuntimeStat;

public class WsStatisticsTask extends TimerTask{
	private WebServiceStatManage webServiceStatManage = null;
	
	public WsStatisticsTask(WebServiceStatManage webServiceStatManage){
		this.webServiceStatManage = webServiceStatManage;
	}
	
	@Override
	public void run() {
		ConcurrentHashMap<String,WSRuntimeStat> runtimeStats = WSRuntimeAnalysis.geInstance(true).getTransitRuntimeStatMap();
		webServiceStatManage.insertStatRecords(runtimeStats);
	}
}
