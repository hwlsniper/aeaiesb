package com.agileai.esb.smc.bizmoduler.tmp;

import java.util.List;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class TmpResourceManageImpl extends StandardServiceImpl implements
		TmpResourceManage {
	
	public void updateContent(DataParam param) {
		String statementId = this.sqlNameSpace + "." + "updateContent";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public List<DataRow> findResGrpRecords(DataParam param) {
		String statementId = this.sqlNameSpace + "." + "findResGrpRecords";
		return daoHelper.queryRecords(statementId, param);
	}
}