package com.agileai.esb.smc.bizmoduler.sysres;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class SkAppManageImpl
        extends StandardServiceImpl
        implements SkAppManage {
    public SkAppManageImpl() {
        super();
    }
     
	public List<DataRow> findTreeGroupRecords(String groupType,String appId) {
		String statementId = sqlNameSpace+"."+"findTreeGroupRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, new DataParam("groupType",groupType,"appId",appId));
		return result;
	}

	public List<DataRow> findMFProfileRecords(String appId) {
		String statementId = sqlNameSpace+"."+"findMFProfileRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, new DataParam("appId",appId));
		return result;
	}

	public List<DataRow> findWSProfileRecords(String appId) {
		String statementId = sqlNameSpace+"."+"findWSProfileRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, new DataParam("appId",appId));
		return result;
	}
	
	public void insertTreeGroupRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertTreeGroupRecord";
		this.daoHelper.insertRecord(statementId, param);
	}

	public void updateTreeGroupRecord(String fid,String name) {
		String statementId = sqlNameSpace+"."+"updateTreeGroupRecord";
		DataParam param = new DataParam();
		param.put("NAME",name);
		param.put("FID",fid);
		this.daoHelper.updateRecord(statementId, param);
	}

	public void deleteTreeGroupRecord(String appId,String fid) {
		String statementId = sqlNameSpace+"."+"deleteTreeGroupRecord";
		DataParam param = new DataParam();
		param.put("FID",fid,"APP_ID",appId);
		this.daoHelper.deleteRecords(statementId, param);
	}
	
	public void deleteTreeGroupRecords(String appId) {
		String statementId = sqlNameSpace+"."+"deleteTreeGroupRecord";
		DataParam param = new DataParam();
		param.put("APP_ID",appId);
		this.daoHelper.deleteRecords(statementId, param);
	}

	public void insertMFProfileRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"insertMFProfileRecord";
		this.daoHelper.insertRecord(statementId, param);
	}
	
	public void updateMFProfileRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateMFProfileRecord";
		this.daoHelper.insertRecord(statementId, param);
	}	
	
	public void deleteMFProfileRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteMFProfileRecord";
		this.daoHelper.deleteRecords(statementId, param);
	}
	
	public void insertWSProfileRecord(DataParam param){
		String statementId = sqlNameSpace+"."+"insertWSProfileRecord";
		this.daoHelper.insertRecord(statementId, param);
	}
	public void deleteWSProfileRecord(DataParam param){
		String statementId = sqlNameSpace+"."+"deleteWSProfileRecord";
		this.daoHelper.deleteRecords(statementId, param);
	}
	
	public void deleteWSProxyRecord(DataParam param){
		String statementId = sqlNameSpace+"."+"deleteWSProxyRecord";
		this.daoHelper.deleteRecords(statementId, param);
	}

	public void updateAppDeploySetting(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateAppDeploySetting";
		this.daoHelper.deleteRecords(statementId, param);
	}

	public void updateMFDeploySetting(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateMFDeploySetting";
		this.daoHelper.deleteRecords(statementId, param);
	}

	public void updateWSDeploySetting(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateWSDeploySetting";
		this.daoHelper.deleteRecords(statementId, param);
	}

	@Override
	public List<DataRow> findMFProfileRecords(String appName, String flowType) {
		String statementId = sqlNameSpace+"."+"findMFProfileRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, new DataParam("appName",appName,"flowType",flowType));
		return result;
	}

	@Override
	public void moveMessageFlowProfiles(String appId,String messageFlowId, String targetFolderId) {
		String statementId = sqlNameSpace+"."+"moveMessageFlowProfile";
		DataParam param = new DataParam("APP_ID",appId,"MF_GROUP",targetFolderId);
		param.put("MF_ID",messageFlowId);
		this.daoHelper.updateRecord(statementId, param);	
	}

	@Override
	public List<String> findPropertiesKeyList() {
		List<String> result = new ArrayList<String>();
		String statementId = "vardefine.findMasterRecords";
		List<DataRow> records = this.daoHelper.queryRecords(statementId, new DataParam());
		if (records != null){
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String varCode = row.stringValue("VAR_CODE");
				result.add(varCode);
			}			
		}
		return result;
	}

	@Override
	public List<DataRow> findAllTreeRecords() {
    	String statementId = sqlNameSpace+"."+"findMenuTreeRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, new DataParam());
		return result;
	}

	@Override
	public List<DataRow> findAuthTreeRecords(String userId) {
    	String statementId = sqlNameSpace+"."+"findAuthMenuTreeRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, new DataParam("USER_ID",userId));
		return result;
	}
	
	public List<DataRow> findTreeRecords(String userId) {
		List<DataRow> allMenus = this.findAllTreeRecords();
		List<DataRow> authMenus = this.findAuthTreeRecords(userId);
		List<DataRow> viewMenus = new ArrayList<DataRow>();
		LinkedHashMap<String, DataRow> menuMap = new LinkedHashMap<String, DataRow>();
		
		if (userId != null && userId.equals("admin")) {
			return allMenus;
		}
		
		if (authMenus != null && authMenus.size() > 0){
			DataRow row = new DataRow();
			row.put("FUNC_ID","C8CD0F2E-FE4B-4EDA-9C0D-858B338BF985");
			row.put("FUNC_NAME","控制台");
			row.put("FUNC_TYPE","funcmenu");
			row.put("FUNC_PID",null);
			authMenus.add(row);
		}
		
		for (int i = 0; i < authMenus.size(); i++) {
			DataRow dataRow = authMenus.get(i);
			String menuId = dataRow.stringValue("FUNC_ID");
			if(hasChildNode(allMenus, menuId)){
				if(hasChildNode(authMenus, menuId)){
					List<DataRow> childNodes = new ArrayList<DataRow>();
					getChildNodes(authMenus, childNodes, menuId);
					
					for (int j = 0; j < childNodes.size(); j++) {
						DataRow childRow = childNodes.get(j);
						String childFunId = childRow.stringValue("FUNC_ID");
						if (!menuMap.containsKey(childFunId)) {
							viewMenus.add(childRow);
							menuMap.put(childFunId, childRow);
						}
					}
				} else {
					List<DataRow> childNodes = new ArrayList<DataRow>();
					getChildNodes(allMenus, childNodes, menuId);
					
					for (int j = 0; j < childNodes.size(); j++) {
						DataRow childRow = childNodes.get(j);
						String childFunId = childRow.stringValue("FUNC_ID");
						if (!menuMap.containsKey(childFunId)) {
							viewMenus.add(childRow);
							menuMap.put(childFunId, childRow);
						}
					}
				}
			}
			if (!menuMap.containsKey(menuId)) {
				viewMenus.add(dataRow);
				menuMap.put(menuId, dataRow);
			}
		}
		return viewMenus;
	}

	private void getChildNodes(List<DataRow> menus, List<DataRow> childMenus, String funId) {
		List<DataRow> childNodes = getChildNodes(menus, funId);
        if (!childNodes.isEmpty()) {
            for (int i = 0; i < childNodes.size(); i++) {
				DataRow dataRow = childNodes.get(i);
				String _funId = dataRow.stringValue("FUNC_ID");
				getChildNodes(menus, childMenus, _funId);
            }  
        }
        childMenus.addAll(childNodes);
	}
	
	private List<DataRow> getChildNodes(List<DataRow> menus, String funId) {
		List<DataRow> childNodes = new ArrayList<DataRow>();
		for (int i = 0; i < menus.size(); i++) {
			DataRow dataRow = menus.get(i);
			String funPid = dataRow.stringValue("FUNC_PID");
			if (funPid.equals(funId)) {
				childNodes.add(dataRow);
			}
		}
		return childNodes;
	}

	private boolean hasChildNode(List<DataRow> authMenus, String funId) {
		for (int i = 0; i < authMenus.size(); i++) {
			DataRow dataRow = authMenus.get(i);
			String funPid = dataRow.stringValue("FUNC_PID");
			if (funPid.equals(funId)) {
				return true;
			}
		}
		return false;
	}
	
	public void deletAuthRecords(String type,String resourceId){
		DataParam delParam = new DataParam();
		String statementId = "sysuser.deleteUserAuthRecord";
		if ("APP".equals(type)){
			delParam.put("AUTH_FUNC_APP_ID",resourceId);			
		}else{
			delParam.put("AUTH_FUNC_ID",resourceId);
		}
		daoHelper.deleteRecords(statementId, delParam);
	}
	
}