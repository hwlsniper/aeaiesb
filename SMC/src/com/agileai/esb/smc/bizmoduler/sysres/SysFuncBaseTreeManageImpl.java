package com.agileai.esb.smc.bizmoduler.sysres;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;

public class SysFuncBaseTreeManageImpl
        extends TreeManageImpl
        implements SysFuncBaseTreeManage {
	public static HashMap<String,List<String>> HandlerIdListCache = new HashMap<String,List<String>>();
	
    public SysFuncBaseTreeManageImpl() {
        super();
        this.idField = "FUNC_ID";
        this.nameField = "FUNC_NAME";
        this.parentIdField = "FUNC_PID";
        this.sortField = "FUNC_SORT";
    }

	@Override
	public HashMap<String, List<String>> getHandlerIdMap() {
		if (HandlerIdListCache.isEmpty()){
			String statementId = this.sqlNameSpace+"."+"getHandlerRecords";
			List<DataRow> records = this.daoHelper.queryRecords(statementId, new DataParam());
			if (records != null && records.size() > 0){
				for (DataRow dataRow : records) {
					String functionId = dataRow.getString("FUNC_ID");
					String handlerId = dataRow.getString("HANLER_CODE");
					List<String> handlerIdList = getHandlerIdList(functionId);
					handlerIdList.add(handlerId);
				}
			}
		}
		return HandlerIdListCache;
	}
    
	private List<String> getHandlerIdList(String functionId){
		if (!HandlerIdListCache.containsKey(functionId)){
			List<String> handlerIdList = new ArrayList<String>();
			HandlerIdListCache.put(functionId, handlerIdList);
		}
		return HandlerIdListCache.get(functionId);
	}
}
