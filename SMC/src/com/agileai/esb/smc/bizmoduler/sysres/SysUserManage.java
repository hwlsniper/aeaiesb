package com.agileai.esb.smc.bizmoduler.sysres;

import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface SysUserManage
        extends StandardService {
	DataRow getRecordByCode(DataParam param);
	public void updateUserPassword(DataParam param);
	boolean saveUserAuth(DataParam param);
	List<DataRow> findAuthCheckeds(DataParam param);
	HashMap<String,DataRow> findAuthAppMap(String userId);
}
