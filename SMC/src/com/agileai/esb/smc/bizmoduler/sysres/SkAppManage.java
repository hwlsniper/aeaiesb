package com.agileai.esb.smc.bizmoduler.sysres;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface SkAppManage
        extends StandardService {
	public List<DataRow> findTreeRecords(String userId);
	public List<DataRow> findAllTreeRecords();
	public List<DataRow> findAuthTreeRecords(String userId);
	
	public List<DataRow> findTreeGroupRecords(String treeGroupType,String appId);
	public List<DataRow> findMFProfileRecords(String appId);
	public List<DataRow> findWSProfileRecords(String appId);
	
	public List<DataRow> findMFProfileRecords(String appName,String flowType);
	public List<String> findPropertiesKeyList();
	
	public void insertTreeGroupRecord(DataParam param);
	public void updateTreeGroupRecord(String fid,String name);
	public void deleteTreeGroupRecord(String appId,String fid);
	public void deleteTreeGroupRecords(String appId);
	
	public void insertMFProfileRecord(DataParam param);
	public void updateMFProfileRecord(DataParam param);
	public void deleteMFProfileRecord(DataParam param);
	
	public void insertWSProfileRecord(DataParam param);
	public void deleteWSProfileRecord(DataParam param);
	
	public void deleteWSProxyRecord(DataParam param);
	
	public void updateAppDeploySetting(DataParam param);
	public void updateMFDeploySetting(DataParam param);
	public void updateWSDeploySetting(DataParam param);
	
	public void moveMessageFlowProfiles(String appId,String messageFlowId,String targetFolderId);
	
	public void deletAuthRecords(String type,String resourceId);
}
