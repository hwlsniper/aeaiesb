package com.agileai.esb.smc.bizmoduler.sysres;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class WsProxyManageImpl
        extends StandardServiceImpl
        implements WsProxyManage {
    public WsProxyManageImpl() {
        super();
    }

	@Override
	public DataRow getAppRecord(String appId) {
		String statementId = sqlNameSpace+"."+"getAppRecord";
		DataParam param = new DataParam("appId",appId);
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
}
