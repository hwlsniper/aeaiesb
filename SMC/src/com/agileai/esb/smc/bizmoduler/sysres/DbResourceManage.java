package com.agileai.esb.smc.bizmoduler.sysres;

import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface DbResourceManage
        extends StandardService {
	public boolean initializeDataSources();
}
