package com.agileai.esb.smc.bizmoduler.sysres;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class MailResourceManageImpl
        extends StandardServiceImpl
        implements MailResourceManage,GenResourceLoader {
	
	public MailResourceManageImpl() {
        super();
    }
    
	@Override
	public DataRow retrieveResourceRow(String resCode) {
		DataParam queryParam = new DataParam("RES_CODE",resCode);
		String statementId = sqlNameSpace+"."+"getRecord";
		DataRow result = this.daoHelper.getRecord(statementId, queryParam);
		return result;
	}

	@Override
	public List<DataRow> retrieveResourceRecords() {
		List<DataRow> records = this.findRecords(new DataParam());
		return records;
	}    
}