package com.agileai.esb.smc.bizmoduler.sysres;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface WsProxyManage
        extends StandardService {
	public DataRow getAppRecord(String appId);
}
