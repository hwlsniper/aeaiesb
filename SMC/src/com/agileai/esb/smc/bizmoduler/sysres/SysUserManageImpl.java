package com.agileai.esb.smc.bizmoduler.sysres;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.hotweb.common.DaoHelper;

public class SysUserManageImpl
        extends StandardServiceImpl
        implements SysUserManage {
    public SysUserManageImpl() {
        super();
    }
	public DataRow getRecordByCode(DataParam param) {
		String statementId = sqlNameSpace+"."+"getRecordByCode";
		DaoHelper daoHelper = this.getDaoHelper();
		DataRow result = daoHelper.getRecord(statementId, param);
		return result;
	}
	
	@Override
	public void updateUserPassword(DataParam param) {
		String statementId = sqlNameSpace + ".updateUserPassword";
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public boolean saveUserAuth(DataParam param) {
		String statementId = sqlNameSpace + ".deleteUserAuthRecord";
		daoHelper.deleteRecords(statementId, param);
		
		String funcIds = param.getString("FUNC_IDS");
		String[] funcArr = funcIds.split(",");
		List<DataParam> paramList = new ArrayList<DataParam>();
		for (int i = 0; i < funcArr.length; i++) {
			DataParam dataParam = new DataParam();
			dataParam.put("AUTH_ID", KeyGenerator.instance().genKey());
			dataParam.put("AUTH_FUNC_ID", funcArr[i]);
			dataParam.put("AUTH_USER_ID", param.get("AUTH_USER_ID"));
			paramList.add(dataParam);
		}
		
		statementId = sqlNameSpace+".insertUserAuthRecord";
		daoHelper.batchInsert(statementId, paramList);
		return true;
	}

	public void deletRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteRecord";
		this.daoHelper.deleteRecords(statementId, param);
		
		statementId = sqlNameSpace + ".deleteUserAuthRecord";
		DataParam delParam = new DataParam("AUTH_USER_ID",param.get("USER_ID"));
		daoHelper.deleteRecords(statementId, delParam);
	}
	
	@Override
	public List<DataRow> findAuthCheckeds(DataParam param) {
		String statementId = sqlNameSpace+".findAuthCheckeds";
		List<DataRow> dataRow = daoHelper.queryRecords(statementId, param);
		return dataRow;
	}
	@Override
	public HashMap<String,DataRow> findAuthAppMap(String userId) {
		HashMap<String,DataRow> result = new HashMap<String,DataRow>();
		String statementId = sqlNameSpace+".findAuthAppRecords";
		DataParam queryParam = new DataParam("userId",userId);
		result = daoHelper.queryRecords("FUNC_NAME",statementId, queryParam);
		return result;
	}
}
