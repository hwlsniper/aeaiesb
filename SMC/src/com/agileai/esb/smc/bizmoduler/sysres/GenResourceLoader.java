package com.agileai.esb.smc.bizmoduler.sysres;

import java.util.List;

import com.agileai.domain.DataRow;

public interface GenResourceLoader {
	DataRow retrieveResourceRow(String resCode); 
	List<DataRow> retrieveResourceRecords();
}
