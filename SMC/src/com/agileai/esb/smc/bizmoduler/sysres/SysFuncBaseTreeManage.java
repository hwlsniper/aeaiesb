package com.agileai.esb.smc.bizmoduler.sysres;

import java.util.HashMap;
import java.util.List;

import com.agileai.hotweb.bizmoduler.core.TreeManage;

public interface SysFuncBaseTreeManage
        extends TreeManage {
	public HashMap<String,List<String>> getHandlerIdMap();
}
