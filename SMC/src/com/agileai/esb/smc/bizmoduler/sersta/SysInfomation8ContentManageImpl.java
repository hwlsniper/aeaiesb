package com.agileai.esb.smc.bizmoduler.sersta;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManageImpl;

public class SysInfomation8ContentManageImpl
        extends TreeAndContentManageImpl
        implements SysInfomation8ContentManage {
    public SysInfomation8ContentManageImpl() {
        super();
        this.columnIdField = "SYSINFO_ID";
        this.columnParentIdField = "SYSINFO_PID";
        this.columnSortField = "SYSINFO_SORT";
    }

	@Override
	public void addService(DataParam dataParam) {
		String statementId = sqlNameSpace+"."+"insertServiceInfomationRelation";
		this.daoHelper.insertRecord(statementId, dataParam);
	}

	@Override
	public List<DataRow> findServiceRecordsBySysinfoId(DataParam param) {
		String statementId = this.sqlNameSpace+"."+"findServiceInfomationRecords";
		return this.daoHelper.queryRecords(statementId, param);
	}

	@Override
	public List<DataRow> queryAppProjectRecords() {
		String statementId = this.sqlNameSpace+"."+"findAppProjectRecords";
		DataParam param = new DataParam();
		return this.daoHelper.queryRecords(statementId, param);
	}
}
