package com.agileai.esb.smc.bizmoduler.sersta;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface CallSysInfomation8ContentManage
        extends TreeAndContentManage {

	void addService(DataParam dataParam);

	List<DataRow> findServiceRecordsBySysinfoId(DataParam param);

	List<DataRow> queryAppProjectRecords();
}
