package com.agileai.esb.smc.bizmoduler.sersta;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.PickFillModelServiceImpl;

public class CallServiceInfomationSelectImpl
        extends PickFillModelServiceImpl
        implements CallServiceInfomationSelect {
    public CallServiceInfomationSelectImpl() {
        super();
    }
    public List<DataRow> queryPickFillRecords(DataParam param) {
		String statementId = "CallServiceInfomationSelect"+"."+queryPickFillRecordsSQL;
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
