package com.agileai.esb.smc.bizmoduler.analysis;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.agileai.domain.DataRow;
import com.agileai.esb.wsmodel.WSRuntimeStat;
import com.agileai.hotweb.bizmoduler.core.BaseInterface;

public interface WebServiceStatManage
        extends BaseInterface {
	public List<DataRow> findStatisticsRecords(String sdate,String edate,String appName,String serviceCode,String authedAppNames);
	public List<DataRow> findTopActiveRecords(String sdate,String edate,int topNum,String appName,String serviceCode,String authedAppNames);
	public List<DataRow> findTopSlowlyRecords(String sdate,String edate,int topNum,String appName,String serviceCode,String authedAppNames);
	
	public List<DataRow> findWsErrorRecords(String sdate,String edate,String appName,String serviceId);
	
	public void insertStatRecords(ConcurrentHashMap<String,WSRuntimeStat> runtimeStats);
	public List<DataRow> findAppRecords();
	public List<DataRow> findStatisticsRecords(String sdate, String edate,
			String appName, int page, int rows,String serviceCode,String authedAppNames);
}
