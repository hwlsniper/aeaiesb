package com.agileai.esb.smc.bizmoduler.analysis;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.wsmodel.WSErrorMsg;
import com.agileai.esb.wsmodel.WSRuntimeStat;
import com.agileai.hotweb.bizmoduler.core.BaseService;

public class WebServiceStatManageImpl
        extends BaseService
        implements WebServiceStatManage {
	
    public WebServiceStatManageImpl() {
        super();
    }

	@Override
	public void insertStatRecords(
			ConcurrentHashMap<String, WSRuntimeStat> runtimeStats) {
		List<DataParam> statRecords = new ArrayList<DataParam>();
		List<DataParam> errorRecords = new ArrayList<DataParam>();
		Enumeration<WSRuntimeStat> enu = runtimeStats.elements();
		while (enu.hasMoreElements()){
			WSRuntimeStat runtimeStat = enu.nextElement();
			DataParam dataParam = new DataParam();
			dataParam.put("STAT_ID",runtimeStat.getStatId());
			dataParam.put("WS_ID",runtimeStat.getServiceId());
			dataParam.put("WS_NAME",runtimeStat.getServiceName());
			dataParam.put("APP_NAME",runtimeStat.getAppName());
			dataParam.put("WS_ALIAS",runtimeStat.getServiceAlias());
			dataParam.put("WS_OPERNAME",runtimeStat.getOperationName());
			dataParam.put("WS_TYPE",runtimeStat.getServiceType());
			dataParam.put("WS_MONITOR_TIME",runtimeStat.getStartime());
			dataParam.put("WS_GATHER_TIME",new Date());
			dataParam.put("WS_RUN_COUNT",runtimeStat.getTotalCount());
			dataParam.put("WS_ERROR_COUNT",runtimeStat.getFailureCount());
			dataParam.put("WS_SUCCESS_COUNT",runtimeStat.getSuccessCount());
			dataParam.put("WS_MAX_RUNTIME",runtimeStat.getMaxRuntime());
			dataParam.put("WS_MIN_RUNTIME",runtimeStat.getMinRuntime());
			dataParam.put("WS_TOTAL_RUNTIME",runtimeStat.getTotalRuntime());
			statRecords.add(dataParam);
			this.processErrorRecords(errorRecords, runtimeStat);
		}
		String statementId = this.sqlNameSpace + ".insertStatRecord";
		this.daoHelper.batchInsert(statementId, statRecords);
		
		statementId = this.sqlNameSpace + ".insertErrorRecord";
		this.daoHelper.batchInsert(statementId, errorRecords);
	}
	
	private void processErrorRecords(List<DataParam> errorRecords,WSRuntimeStat runtimeStat){
		List<WSErrorMsg> errorMessages = runtimeStat.getErrorMessages();
		for (int i = 0;i < errorMessages.size();i++){
			WSErrorMsg errorMsg = errorMessages.get(i);

			DataParam errorParam = new DataParam();
			errorParam.put("STAT_ID",runtimeStat.getStatId());
			errorParam.put("WS_ID",runtimeStat.getServiceId());
			errorParam.put("WS_OPERNAME",runtimeStat.getOperationName());
			errorParam.put("APP_NAME",runtimeStat.getAppName());
			errorParam.put("ERROR_TIME",errorMsg.getStartTime());	
			errorParam.put("ERROR_INFO",errorMsg.getMessageBody());	
			errorParam.put("REQ_SOAP_MSG",errorMsg.getReqSoapXML());

			errorRecords.add(errorParam);
		}
	}

	@Override
	public List<DataRow> findStatisticsRecords(String sdate, String edate,String appName
			,String serviceCode,String authedAppNames) {
		DataParam param = new DataParam();
		String statementId = this.sqlNameSpace+".findStatisticsRecords";
		String startTime = sdate+" 00:00:00";
		String endTime = edate+" 23:59:59";
		param.put("startTime",startTime);
		param.put("endTime",endTime);
		param.put("appName",appName);
		param.put("authedAppNames",authedAppNames);
		param.put("authedAppNames",authedAppNames);
		
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public List<DataRow> findTopActiveRecords(String sdate, String edate,
			int topNum,String appName,String serviceCode,String authedAppNames) {
		String statementId = this.sqlNameSpace+".findTopActiveRecords";
		DataParam param = new DataParam();
		String startTime = sdate+" 00:00:00";
		String endTime = edate+" 23:59:59";
		param.put("startTime",startTime);
		param.put("endTime",endTime);
		param.put("appName",appName);
		
		param.put("serviceCode",serviceCode);
		param.put("authedAppNames",authedAppNames);
		
		List<DataRow> records = new ArrayList<DataRow>();
		List<DataRow> tempRecords = this.daoHelper.queryRecords(statementId, param);
		if (tempRecords != null){
			int size = topNum > tempRecords.size() ? tempRecords.size():topNum;
			for (int i = 0;i < size;i++){
				DataRow dataRow = tempRecords.get(i);
				records.add(dataRow);
			}
		}
		return records;
	}

	@Override
	public List<DataRow> findTopSlowlyRecords(String sdate, String edate,
			int topNum,String appName,String serviceCode,String authedAppNames) {
		String statementId = this.sqlNameSpace+".findTopSlowlyRecords";
		DataParam param = new DataParam();
		String startTime = sdate+" 00:00:00";
		String endTime = edate+" 23:59:59";
		param.put("startTime",startTime);
		param.put("endTime",endTime);
		param.put("appName",appName);
		
		param.put("serviceCode",serviceCode);
		param.put("authedAppNames",authedAppNames);
		
		List<DataRow> records = new ArrayList<DataRow>();
		List<DataRow> tempRecords = this.daoHelper.queryRecords(statementId, param);
		if (tempRecords != null){
			int size = topNum > tempRecords.size() ? tempRecords.size():topNum;
			for (int i = 0;i < size;i++){
				DataRow dataRow = tempRecords.get(i);
				records.add(dataRow);
			}
		}
		return records;
	}

	@Override
	public List<DataRow> findWsErrorRecords(String sdate, String edate,String appName,
			String serviceId) {
		String statementId = this.sqlNameSpace+".findWsErrorRecords";
		DataParam param = new DataParam();
		String startTime = sdate+" 00:00:00";
		String endTime = edate+" 23:59:59";
		param.put("startTime",startTime);
		param.put("endTime",endTime);
		param.put("appName",appName);
		param.put("serviceId",serviceId);
		List<DataRow> tempRecords = this.daoHelper.queryRecords(statementId, param);
		return tempRecords;
	}

	@Override
	public List<DataRow> findAppRecords() {
		String statementId = this.sqlNameSpace+".findAppRecords";
		DataParam param = new DataParam();
		List<DataRow> appRecords = this.daoHelper.queryRecords(statementId, param);
		return appRecords;
	}

	@Override
	public List<DataRow> findStatisticsRecords(String sdate, String edate,
			String appName, int page, int rows,String serviceCode,String authedAppNames) {
		String statementId = this.sqlNameSpace+".findStatisticsRecordsByPage";
		String startTime = sdate+" 00:00:00";
		String endTime = edate+" 23:59:59";
		
		DataParam param = new DataParam();
		param.put("startTime",startTime);
		param.put("endTime",endTime);
		param.put("appName",appName);
		param.put("sPage",(page-1)*rows);
		param.put("ePage",page*rows);
		
		param.put("serviceCode",serviceCode);
		param.put("authedAppNames",authedAppNames);
		
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}
}
