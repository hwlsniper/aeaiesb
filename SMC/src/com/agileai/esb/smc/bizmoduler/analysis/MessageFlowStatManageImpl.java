package com.agileai.esb.smc.bizmoduler.analysis;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.esb.component.ErrorMsg;
import com.agileai.esb.component.RuntimeStat;
import com.agileai.hotweb.bizmoduler.core.BaseService;

public class MessageFlowStatManageImpl
        extends BaseService
        implements MessageFlowStatManage {
	
    public MessageFlowStatManageImpl() {
        super();
    }

	@Override
	public void insertStatRecords(
			ConcurrentHashMap<String, RuntimeStat> runtimeStats) {
		List<DataParam> statRecords = new ArrayList<DataParam>();
		List<DataParam> errorRecords = new ArrayList<DataParam>();
		Enumeration<RuntimeStat> enu = runtimeStats.elements();
		while (enu.hasMoreElements()){
			RuntimeStat runtimeStat = enu.nextElement();
			DataParam dataParam = new DataParam();
			dataParam.put("STAT_ID",runtimeStat.getStatId());
			dataParam.put("MF_ID",runtimeStat.getMessageFlowId());
			dataParam.put("MF_NAME",runtimeStat.getMessageFlowName());
			dataParam.put("APP_NAME",runtimeStat.getAppName());
			dataParam.put("MF_ALIAS",runtimeStat.getMessageFlowAlias());
			dataParam.put("MF_TYPE",runtimeStat.getMessageFlowType());
			dataParam.put("MF_MONITOR_TIME",runtimeStat.getStartime());
			dataParam.put("MF_GATHER_TIME",new Date());
			dataParam.put("MF_RUN_COUNT",runtimeStat.getTotalCount());
			dataParam.put("MF_ERROR_COUNT",runtimeStat.getFailureCount());
			dataParam.put("MF_SUCCESS_COUNT",runtimeStat.getSuccessCount());
			dataParam.put("MF_MAX_RUNTIME",runtimeStat.getMaxRuntime());
			dataParam.put("MF_MIN_RUNTIME",runtimeStat.getMinRuntime());
			dataParam.put("MF_TOTAL_RUNTIME",runtimeStat.getTotalRuntime());
			statRecords.add(dataParam);
			this.processErrorRecords(errorRecords, runtimeStat);
		}
//		#STAT_ID#,#MF_ID#,#MF_NAME#,#MF_ALIAS#,#MF_TYPE#,#MF_MONITOR_TIME#,#MF_GATHER_TIME#,#MF_RUN_COUNT#,#MF_ERROR_COUNT#,#MF_SUCCESS_COUNT#,#MF_MAX_RUNTIME#,#MF_MIN_RUNTIME#,#MF_TOTAL_RUNTIME#
		String statementId = this.sqlNameSpace + ".insertStatRecord";
		this.daoHelper.batchInsert(statementId, statRecords);
		
//		#STAT_ID#,#MF_ID#,#ERROR_TIME#,#ERROR_INFO#
		statementId = this.sqlNameSpace + ".insertErrorRecord";
		this.daoHelper.batchInsert(statementId, errorRecords);
	}
	
	private void processErrorRecords(List<DataParam> errorRecords,RuntimeStat runtimeStat){
		List<ErrorMsg> errorMessages = runtimeStat.getErrorMessages();
		for (int i = 0;i < errorMessages.size();i++){
			ErrorMsg errorMsg = errorMessages.get(i);
			
			DataParam errorParam = new DataParam();
			errorParam.put("STAT_ID",runtimeStat.getStatId());
			errorParam.put("MF_ID",runtimeStat.getMessageFlowId());
			errorParam.put("APP_NAME",runtimeStat.getAppName());
			errorParam.put("ERROR_TIME",errorMsg.getStartTime());
			errorParam.put("ERROR_INFO",errorMsg.getMessageBody());	
			errorRecords.add(errorParam);
		}
	}

	@Override
	public List<DataRow> findStatisticsRecords(String sdate, String edate,String appName) {
		String statementId = this.sqlNameSpace+".findStatisticsRecords";
		DataParam param = new DataParam();
		String startTime = sdate+" 00:00:00";
		String endTime = edate+" 23:59:59";
		param.put("startTime",startTime);
		param.put("endTime",endTime);
		param.put("appName",appName);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}

	@Override
	public List<DataRow> findTopActiveRecords(String sdate, String edate,
			int topNum,String appName,String authedAppNames) {
		String statementId = this.sqlNameSpace+".findTopActiveRecords";
		DataParam param = new DataParam();
		String startTime = sdate+" 00:00:00";
		String endTime = edate+" 23:59:59";
		param.put("startTime",startTime);
		param.put("endTime",endTime);
		param.put("appName",appName);
		param.put("authedAppNames",authedAppNames);
		
		List<DataRow> records = new ArrayList<DataRow>();
		List<DataRow> tempRecords = this.daoHelper.queryRecords(statementId, param);
		if (tempRecords != null){
			int size = topNum > tempRecords.size() ? tempRecords.size():topNum;
			for (int i = 0;i < size;i++){
				DataRow dataRow = tempRecords.get(i);
				records.add(dataRow);
			}
		}
		return records;
	}

	@Override
	public List<DataRow> findTopSlowlyRecords(String sdate, String edate,
			int topNum,String appName,String authedAppNames) {
		String statementId = this.sqlNameSpace+".findTopSlowlyRecords";
		DataParam param = new DataParam();
		String startTime = sdate+" 00:00:00";
		String endTime = edate+" 23:59:59";
		param.put("startTime",startTime);
		param.put("endTime",endTime);
		param.put("appName",appName);
		param.put("authedAppNames",authedAppNames);
		List<DataRow> records = new ArrayList<DataRow>();
		List<DataRow> tempRecords = this.daoHelper.queryRecords(statementId, param);
		if (tempRecords != null){
			int size = topNum > tempRecords.size() ? tempRecords.size():topNum;
			for (int i = 0;i < size;i++){
				DataRow dataRow = tempRecords.get(i);
				records.add(dataRow);
			}
		}
		return records;
	}

	@Override
	public List<DataRow> findMFErrorRecords(String sdate, String edate,String appName,
			String messageFlowId) {
		String statementId = this.sqlNameSpace+".findMFErrorRecords";
		DataParam param = new DataParam();
		String startTime = sdate+" 00:00:00";
		String endTime = edate+" 23:59:59";
		param.put("startTime",startTime);
		param.put("endTime",endTime);
		param.put("appName",appName);
		param.put("messageFlowId",messageFlowId);
		List<DataRow> tempRecords = this.daoHelper.queryRecords(statementId, param);
		return tempRecords;
	}

	@Override
	public List<DataRow> findAppRecords() {
		String statementId = this.sqlNameSpace+".findAppRecords";
		DataParam param = new DataParam();
		List<DataRow> appRecords = this.daoHelper.queryRecords(statementId, param);
		return appRecords;
	}

	@Override
	public List<DataRow> findStatisticsRecords(String sdate, String edate,
			String appName, int page, int rows,DataParam param) {
		String statementId = this.sqlNameSpace+".findStatisticsRecordsByPage";
		String startTime = sdate+" 00:00:00";
		String endTime = edate+" 23:59:59";
		param.put("startTime",startTime);
		param.put("endTime",endTime);
		param.put("appName",appName);
		param.put("sPage",(page-1)*rows);
		param.put("ePage",page*rows);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		return records;
	}
}
