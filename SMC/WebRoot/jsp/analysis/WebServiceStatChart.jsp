<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>运行状态监控</title>
<script type="text/javascript" src="js/FusionCharts_pc.js"></script>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link rel="stylesheet" href="css/skin/china/skinstyle.css"  type="text/css" />
<link rel="stylesheet" href="css/skin/vista/skinstyle.css" type="text/css" />
<link rel="stylesheet" href="css/skin/mac/skinstyle.css" type="text/css" />
<link rel="stylesheet" href="css/icon.css" type="text/css" />
<link rel="stylesheet" href="css/easyui.css" type="text/css" />	
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>	
<script type="text/javascript" src="js/jquery.pagination.js"></script>	
<script type="text/javascript">
var errorBox;
function showErrorBox(appName,serviceId){
	//clearSelection();
	if (!errorBox){
		errorBox = new PopupBox('errorBox','错误信息查看',{size:'big',height:'550px',top:'30px'});
	}
	var url = "index?WSRuntimeErrors&actionType=showHistoryError&appName="+appName+"&serviceId="+serviceId+"&sdate="+$("#sdate").val()+"&edate="+$("#edate").val();
	errorBox.sendRequest(url);
}

function frameHeight(){  
	var height = $(window).height() - 350;
	return height;
}
function frameWidth(){  
	var width = $(window).width() - 10;
	return width;
} 

function resetServiceRequest(){
	$('#serCode').val("");
	doQuery();
}

$(document).ready(function() {
	$('#dg').datagrid({  
        width: 'auto',  
        height:frameHeight(),              
        //striped: true,  
        singleSelect : true, 
        url:'<%=pageBean.getHandlerURL()%>&actionType=loadStatData&appName='+$("#appName").val()+'&sdate='+$("#sdate").val()+'&edate='+$("#edate").val()+'&serCode='+$("#serCode").val(), 
        //queryParams:{},  
        idField:'messageFlowId',
        loadMsg:'数据加载中请稍后……',  
        pagination:true,//显示分页  
	   	rownumbers:true,//行号 
	   	//fit:true,//自动补全  
        fitColumns:true,
        columns:[[
      	        {field:'appName',title:'工程',width:100,align:'right'},
      	        {field:'webServeiceName',title:'编码',width:100,align:'right'},
      	        {field:'webServeiceAlias',title:'别名',width:100,align:'right'},
      	      	{field:'webServeiceOperation',title:'方法',width:100,align:'right'},
      	        {field:'webServeiceType',title:'服务类型',width:100,align:'right'},
      	        {field:'totalCount',title:'运行次数',width:100,align:'right'},
      	        {field:'successCount',title:'成功次数',width:100,align:'right'},
      	        {field:'failureCount',title:'失败次数',width:100,align:'right'},
      	        {field:'maxRuntime',title:'最长时间',width:100,align:'right'},
      	        {field:'minRuntime',title:'最短时间',width:100,align:'right'},
      	        {field:'averageRuntime',title:'平均时间',width:100,align:'right'}
      	    ]],
      	onDblClickRow: function (index,row) {//双击的逻辑和单机的那个差不多  
	  			showErrorBox(row.appName,row.webServeiceId);
          },  
    });
	 var p = $('#dg').datagrid('getPager'); 
	    $(p).pagination({ 
	        pageSize: 10,//每页显示的记录条数，默认为10 
	        pageList: [5,10,15],//可以设置每页记录条数的列表 
	        beforePageText: '第',//页数文本框前显示的汉字 
	        afterPageText: '页    共 {pages} 页', 
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录', 
	        /*onBeforeRefresh:function(){
	            $(this).pagination('loading');
	            alert('before refresh');
	            $(this).pagination('loaded');
	        }*/ 
	    }); 
});
window.onresize = function(){ 
	$('#dg').datagrid({width:frameWidth()}); 
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<div style="height:8px;"></div>
<div>
<table class="queryTable">
<tr><td>
&nbsp;工程列表
<select id="appName" label="工程" name="appName" class="select" onchange="doQuery()"><%=pageBean.selectValue("appName")%></select>
&nbsp;服务编码&nbsp;<input id="serCode" label="服务编码" name="serCode" type="text" value="<%=pageBean.inputValue("serCode")%>" size="24" class="text" ondblclick="resetServiceRequest()"/>
&nbsp;起止日期：<input id="sdate" name="sdate" type="text" value="<%=pageBean.inputTime("sdate")%>" size="16" class="text" /><img id="startTimePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
-<input id="edate" name="edate" type="text" value="<%=pageBean.inputTime("edate")%>" size="16" class="text" /><img id="endTimePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr>
</table>
</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
	<td width="50%"><div id="activeWsStatChart" align="center" style="height:270px;padding:1px;"></div></td>
	<td width="50%"><div id="slowlyWsStatChar" align="center" style="height:270px;padding:1px;"></div></td>
  </tr>
</table>
<table id="dg"></table>  
<input type="hidden" name="actionType" id="actionType" />
</form>
</body>
</html>
<script type="text/javascript">
initCalendar('sdate','%Y-%m-%d','startTimePicker');
initCalendar('edate','%Y-%m-%d','endTimePicker');
$(function(){
	var __loadActiveWsStatChart = function(){
		var chart = new FusionCharts("<%=request.getContextPath()%>/charts/MSColumn3D.swf","activeMfStat","100%","100%", "0", "0","FFFFFF", "exactFit");
		var url = "<%=pageBean.getHandlerURL()%>&actionType=retrieveXml&infoType=ActiveWsStat&appName="+$("#appName").val()+"&sdate="+$("#sdate").val()
			+"&edate="+$("#edate").val() + "&serCode="+$("#serCode").val();
		sendRequest(url,{dataType:'text',onComplete:function(responseText){
			chart.setDataXML(responseText);
			chart.render("activeWsStatChart");
		}});
	};
	__loadActiveWsStatChart();
	var __loadSlowlyWsStatChart = function(){
		var chart = new FusionCharts("<%=request.getContextPath()%>/charts/Column3D.swf","slowlyMfStat","100%","100%", "0", "0","FFFFFF", "exactFit");
		var url = "<%=pageBean.getHandlerURL()%>&actionType=retrieveXml&infoType=SlowlyWsStat&appName="+$("#appName").val()+"&sdate="+$("#sdate").val()
			+"&edate="+$("#edate").val() + "&serCode="+$("#serCode").val();
		sendRequest(url,{dataType:'text',onComplete:function(responseText){
			chart.setDataXML(responseText);
			chart.render("slowlyWsStatChar");
		}});
	};
	__loadSlowlyWsStatChart();
});
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
