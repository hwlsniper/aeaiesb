<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.agileai.hotweb.domain.core.Profile"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String localAddr = request.getLocalAddr();
if ("0.0.0.0".equals(localAddr)){
	localAddr = "localhost";
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>消息流程管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function startMF(){
	showSplash();
	postRequest('form1',{actionType:'startMF',onComplete:function(responseText){
		if (responseText == 'success'){
			doSubmit({actionType:'prepareDisplay'})
		}else{
			writeErrorMsg("启动消息流程失败！");
			hideSplash();
		}
	}});
}
function stopMF(){
	showSplash();
	postRequest('form1',{actionType:'stopMF',onComplete:function(responseText){
		if (responseText == 'success'){
			doSubmit({actionType:'prepareDisplay'})
		}else{
			writeErrorMsg("停止消息流程失败！");
			hideSplash();
		}
	}});
}
var messageFlowImageBox;
function showMessageFlowImageBoxRequest(){
	if (!messageFlowImageBox){
		messageFlowImageBox = new PopupBox('messageFlowImageBox','消息流程图',{size:'big',width:'900px',height:'400px',top:'5px'});
	}
	var url = 'index?MfImageView&mfId=<%=pageBean.inputValue("MF_ID")%>&appId=<%=pageBean.inputValue("APP_ID")%>';
	messageFlowImageBox.sendRequest(url);
}
var invokeBox;
function showInvokeBoxRequest(){
	if (!invokeBox){
		invokeBox = new PopupBox('invokeBox','手工调用',{size:'big',width:'800px',height:'500px',top:'5px'});
	}
	var url = 'index?HttpFlowInvoke&mfId=<%=pageBean.inputValue("MF_ID")%>&appId=<%=pageBean.inputValue("APP_ID")%>';
	invokeBox.sendRequest(url);
}

function goToList() {
	var appId = $("#APP_ID").val();
	var groupId = $("#groupId").val();
	var wsId = $("#wsId").val();
	var url = "/index?MfProfileManageList&appId="+appId+"&groupId="+groupId+"&wsId="+wsId;
	
	var path = "<%=request.getContextPath() %>";
	url = path + url;
	window.location.href = url;
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
<%if (pageBean.isTrue(pageBean.selectedValue("DEPLOYED"))){%>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="showMessageFlowImageBoxRequest();"><input value="&nbsp;" type="button" class="designImgBtn" title="显示流程图" />流程图</td>
<%}%>
<%if (!pageBean.isTrue(pageBean.getStringValue("fromNode"))){%> 
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToList();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
<%}%>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>类型</th>
	<td><input name="MF_TYPE" type="text" class="text" id="MF_TYPE" value="<%=pageBean.inputValue("MF_TYPE")%>" size="24" readonly="readonly" label="类型" /></td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input name="MF_NAME" type="text" class="text" id="MF_NAME" value="<%=pageBean.inputValue("MF_NAME")%>" size="24" readonly="readonly" label="名称" />
<%if ("Http".equalsIgnoreCase(pageBean.inputValue("MF_TYPE"))){%>
<input type="button" class="formbutton" name="invokeButton" id="invokeButton" value="调用" onclick="showInvokeBoxRequest()" />
<%}%>
    </td>
</tr>
<tr>
	<th width="100" nowrap>别名</th>
	<td><input id="MF_ALIAS" label="别名" name="MF_ALIAS" type="text" value="<%=pageBean.inputValue("MF_ALIAS")%>" size="24" class="text editable" />
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td><input name="isActive" type="text" class="text" id="isActive" value="<%=pageBean.inputValue("isActive")%>" size="24" readonly="readonly" label="当前状态" />
	  <input type="button" class="formbutton" name="operaButton" id="operaButton" value="<%=pageBean.inputValue("operaButton")%>" onclick="<%=pageBean.inputValue("operaHandler")%>" /></td>
</tr>
<%if ("Http".equalsIgnoreCase(pageBean.inputValue("MF_TYPE"))){%>
<tr>
	<th width="100" nowrap>地址</th>
	<td><input name="address" type="text" class="text" id="address"readonly="readonly" value="http://<%=localAddr%>:<%=request.getLocalPort()%>/<%=pageBean.inputValue("APP_NAME")%>/http/<%=pageBean.inputValue("MF_NAME")%>" size="65" label="当前状态" /></td>
</tr>
<%}%>
<tr>
	<th width="100" nowrap>共享日志</th>
	<td>&nbsp;<%=pageBean.selectRadio("SHARELOG")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>是否部署</th>
	<td>&nbsp;<%=pageBean.selectRadio("DEPLOYED")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>自动启动</th>
	<td>&nbsp;<%=pageBean.selectRadio("START_WITH_SERVER")%>
</td>
</tr>
<%
if (pageBean.isValid(pageBean.inputValue("WS_ID"))){
%>
<tr>
	<th width="100" nowrap>关联服务</th>
	<td><input name="WS_ALIAS" type="text" class="text" id="WS_ALIAS" value="<%=pageBean.inputValue("WS_ALIAS")%>" size="24" readonly="readonly" label="关联服务" /></td>
</tr>
<%}%>
<tr>
	<th width="100" nowrap>创建人</th>
	<td><input name="CREATE_USER" type="text" class="text" id="CREATE_USER" value="<%=pageBean.inputValue("CREATE_USER")%>" size="24" readonly="readonly" label="创建人" /></td>
</tr>
<tr>
	<th width="100" nowrap>创建时间</th>
	<td><input name="CREATE_TIME" type="text" class="text" id="CREATE_TIME" value="<%=pageBean.inputTime("CREATE_TIME")%>" size="24" readonly="readonly" label="创建时间" /></td>
</tr>
<tr>
	<th width="100" nowrap>部署人</th>
	<td><input name="DEPLOY_USER" type="text" class="text" id="DEPLOY_USER" value="<%=pageBean.inputValue("DEPLOY_USER")%>" size="24" readonly="readonly" label="部署人" /></td>
</tr>
<tr>
	<th width="100" nowrap>部署时间</th>
	<td><input name="DEPLOY_TIME" type="text" class="text" id="DEPLOY_TIME" value="<%=pageBean.inputTime("DEPLOY_TIME")%>" size="24" readonly="readonly" label="部署时间" /></td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><textarea name="MF_DESC" cols="75" rows="3" class="text editable" id="MF_DESC" label="描述"><%=pageBean.inputValue("MF_DESC")%></textarea></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="MF_ID" name="MF_ID" value="<%=pageBean.inputValue4DetailOrUpdate("MF_ID","")%>" />
<input type="hidden" id="APP_NAME" name="APP_NAME" value="<%=pageBean.inputValue("APP_NAME")%>" />
<input type="hidden" id="APP_ID" name="APP_ID" value="<%=pageBean.inputValue("APP_ID")%>" />
<input id="groupId" name="groupId" type="hidden" value="<%=pageBean.inputValue("groupId")%>" />
<input id="wsId" name="wsId" type="hidden" value="<%=pageBean.inputValue("wsId")%>" />
</form>
<script language="javascript">
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
