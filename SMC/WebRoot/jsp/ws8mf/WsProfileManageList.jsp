<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="com.agileai.hotweb.domain.core.Profile"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
	Profile profile = (Profile) request.getSession().getAttribute(Profile.PROFILE_KEY);
	String userId = profile.getUserId();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Web服务管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function startWS(){
	bachProcessIds();
	var ids = $("#ids").val();
	if (ids != ""){
		confirmWsState('startWS');
	}else{
		writeErrorMsg("请先选中一条记录!");
	}
}
function stopWS(){
	bachProcessIds();
	var ids = $("#ids").val();
	if (ids != ""){
		confirmWsState('stopWS');
	}else{
		writeErrorMsg("请先选中一条记录!");
	}
}
function buttonControl(deployed,currentState,indexId){
	if ('Y' == deployed){
		if ('true'==currentState){
			enableButton('stopImgBtn');
			disableButton('startImgBtn');
			disableButton('delImgBtn');
		}else{
			enableButton('startImgBtn');
			disableButton('stopImgBtn');
			enableButton('delImgBtn');
		}
	}else{
		disableButton('startImgBtn');
		disableButton('stopImgBtn');
		enableButton('delImgBtn');
	}
	
	var idInt = parseInt(indexId);
	var currentIndexId = idInt ;
	if($("#ec_table tr:eq("+currentIndexId+") input[name = 'WS_ID']").is(':checked')){
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'WS_ID']").attr('checked',false);
	}else{
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'WS_ID']").attr('checked',true);
	}
}

function doDeleteReq(){	
	var confirmsubMsg="您确认删除该数据吗？";
	bachProcessIds();
	var ids = $("#ids").val();
	if (ids == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	
	if(confirm(confirmsubMsg)) {
		confirmWsState('doDeleteReq');
	}
}

function bachProcessIds(){
	var ids = "";
	$("input:[name = 'WS_ID'][checked]").each(function(){   
		ids = ids+$(this).val()+",";
	});
	if (ids.length > 0){
		ids = ids.substring(0,ids.length-1);
	}
	$("#ids").val(ids);
}

function confirmWsState(resource){
	$("#resource").val(resource);
	postRequest('form1',{actionType:'confirmWsState',onComplete:function(responseText){
		if (responseText == 'doDeleteReq'){
			doSubmit({actionType:'batchDelete'});
		}else if(responseText == 'startWS'){
			showSplash();
			postRequest('form1',{actionType:'startWS',onComplete:function(responseText){
				if (responseText == 'success'){
					doSubmit({actionType:'prepareDisplay'});
				}else{
					writeErrorMsg("启动WEB服务失败！");
					hideSplash();
				}
			}});	
		}else if(responseText == 'stopWS'){
			showSplash();
			postRequest('form1',{actionType:'stopWS',onComplete:function(responseText){
				if (responseText == 'success'){
					doSubmit({actionType:'prepareDisplay'});
				}else{
					writeErrorMsg("停止WEB服务失败！");
					hideSplash();
				}
			}});		
		}else{
			if("doDeleteReq" == resource){
				writeErrorMsg("只有状态为已停止的才能删除，请检查!!");
			}else if("startWS" == resource){
				writeErrorMsg("只有状态为已停止的才能启动，请检查!!");
			}else if("stopWS" == resource){
				writeErrorMsg("只有状态为已启动的才能停止，请检查!!");
			}
		}
	}});
}

function showDetail(actionType,webServiceId) {
	var appId = $("#APP_ID").val();
	var ids = '';
	if(!webServiceId){
		bachProcessIds();
		ids = $("#ids").val();
		if(ids.length > 36){
			writeErrorMsg('只能选中一条记录!');
			return;
		}
		if (ids == ""){
			writeErrorMsg('请先选中一条记录!');
			return;
		}
	}else{
		ids = webServiceId;
	}
	
	var url = "/index?WsProfileManageEdit&operaType=detail&WS_ID="+ids+"&APP_ID="+appId;
	var path = "<%=request.getContextPath() %>";
	url = path + url;
	window.location.href = url;
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">
&nbsp;<input id="groupId" name="groupId" type="hidden" value="<%=pageBean.inputValue("groupId")%>" />
&nbsp;<input id="appId" name="appId" type="hidden" value="<%=pageBean.inputValue("appId")%>" />
</div>
<div id="__ToolBar__">
<span style="float:right;height:28px;line-height:28px;">&nbsp;状态&nbsp;<select id="wsState" label="状态" name="wsState" onchange="doQuery()" class="select"><%=pageBean.selectValue("wsState")%></select></span>
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="showDetail('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" id="detailImgBtn" />查看</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="S" align="center" onclick="startWS()"><input value="&nbsp;" title="查看" type="button" class="startImgBtn" id="startImgBtn" />启动</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="S" align="center" onclick="stopWS()"><input value="&nbsp;" title="查看" type="button" class="stopImgBtn" id="stopImgBtn" />停止</td>      
<%if(userId != null && userId.equals("admin")){ %>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDeleteReq();"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" id="delImgBtn" />删除</td>
<%}%>
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="Web服务管理.csv"
retrieveRowsCallback="process" xlsFileName="Web服务管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();showDetail('viewDetail','${row.WS_ID}')" oncontextmenu="buttonControl('${row.DEPLOYED}','${row.isActiveCode}');selectRow(this,{WS_ID:'${row.WS_ID}',APP_ID:'${row.APP_ID}',APP_NAME:'${row.APP_NAME}',WS_NAME:'${row.WS_NAME}'});refreshConextmenu()" onclick="buttonControl('${row.DEPLOYED}','${row.isActiveCode}','${GLOBALROWCOUNT}');selectRow(this,{WS_ID:'${row.WS_ID}',APP_ID:'${row.APP_ID}',APP_NAME:'${row.APP_NAME}',WS_NAME:'${row.WS_NAME}'})">
	<ec:column width="25" style="text-align:center" property="WS_ID" cell="checkbox" headerCell="checkbox"/>
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="80" property="isActive" title="状态"  style="text-align:center;"  />
	<ec:column width="200" property="WS_NAME" title="名称"   />
	<ec:column width="200" property="WS_ALIAS" title="别名"   />
	<ec:column width="80" property="DEPLOYED" mappingItem="DEPLOYED" title="是否部署" />
	<ec:column width="100" property="WS_VERSION" title="版本"   />
</ec:row>
</ec:table>
<input type="hidden" name="WS_ID" id="WS_ID" value="" />
<input type="hidden" id="APP_ID" name="APP_ID" value="" />
<input type="hidden" id="APP_NAME" name="APP_NAME" value="" />
<input type="hidden" id="WS_NAME" name="WS_NAME" value="" />
<input type="hidden" name="resource" id="resource" />
<input type="hidden" id="ids" name="ids" value="<%=pageBean.inputValue("ids")%>" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('WS_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
<script language="javascript">
<%
if (pageBean.isTrue(pageBean.inputValue("refreshTree"))){
%>
parent.frames["leftFrame"].location.reload();
<%
}
%>
</script>