<%@ page contentType="text/html; charset=UTF-8" errorPage="/jsp/frame/Error.jsp" %>
<%@ page import="com.agileai.esb.smc.domain.User"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String mfId = pageBean.inputValue("mfId");
String appId = pageBean.inputValue("appId");
%>
<html>
<head>
<title>图片显示</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="js/util.js"></script>
</head>
<body style="margin:0px;">
<form id="form1" name="form1" method="post" action="<%=pageBean.getHandlerURL()%>">
<div style="width:890px;height:370px;overflow:auto;">
<img id="messageFlowImage" src="<%=pageBean.getHandlerURL()%>&actionType=showImage&mfId=<%=mfId%>&appId=<%=appId%>" />
</div>
</form>
</body>
</html>