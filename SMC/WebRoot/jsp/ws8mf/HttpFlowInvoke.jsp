<%@ page contentType="text/html; charset=UTF-8" errorPage="/jsp/frame/Error.jsp" %>
<%@ page import="com.agileai.esb.smc.domain.User"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String mfId = pageBean.inputValue("mfId");
String appId = pageBean.inputValue("appId");
String localAddr = request.getLocalAddr();
if ("0.0.0.0".equals(localAddr)){
	localAddr = "localhost";
}
%>
<html>
<head>
<title>图片显示</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript">
function invokeMsgFlow(){
	showSplash();
	postRequest('form1',{actionType:'invokeMsgFlow',onComplete:function(responseText){
		$("#_mfResponse").val(responseText);
		hideSplash();		
	}});	
}
</script>
</head>
<body style="margin:0px;">
<form id="form1" name="form1" method="post" action="<%=pageBean.getHandlerURL()%>" style="padding:5px;">
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input name="_mfName" type="text" class="text" id="_mfName" value="<%=pageBean.inputValue("MF_NAME")%>" size="24" readonly="readonly" label="名称" /></td>
</tr>
<tr>
	<th width="100" nowrap>别名</th>
	<td><input id="_mfAlias" label="别名" name="_mfAlias" type="text" value="<%=pageBean.inputValue("MF_ALIAS")%>" size="24" readonly="readonly" /></td>
</tr>
<tr>
	<th width="100" nowrap>地址</th>
	<td><input name="_mfAddress" type="text" readonly="readonly" id="_mfAddress" value="http://<%=localAddr%>:<%=request.getLocalPort()%>/<%=pageBean.inputValue("APP_NAME")%>/http/<%=pageBean.inputValue("MF_NAME")%>" size="65" label="当前状态" />&nbsp;<input type="button" class="formbutton" name="invokeButton" id="invokeButton" value="调用" onClick="invokeMsgFlow()" /></td>
</tr>
<%
for (int i=0;i < pageBean.getRsList().size();i++){
%>
<tr>
	<th width="100" nowrap><%=pageBean.inputValue(i,"PARAM_CODE")%></th>
	<td><input name="<%=pageBean.inputValue(i,"PARAM_CODE")%>" type="text" class="text editable" id="<%=pageBean.inputValue(i,"PARAM_CODE")%>" value="" size="24" /></td>
</tr>
<%}%>
<tr>
	<th width="100" nowrap>响应输出</th>
	<td><textarea name="_mfResponse" cols="65" rows="7" id="_mfResponse" label="描述"><%=pageBean.inputValue("MF_DESC")%></textarea>
	</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" id="_mfId" name="_mfId" value="<%=pageBean.inputValue("mfId")%>" />
</form>
</body>
</html>