<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>系统信息</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>系统编码</th>
	<td><input id="SYSINFO_CODE" label="系统编码" name="SYSINFO_CODE" type="text" value="<%=pageBean.inputValue("SYSINFO_CODE")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>系统名称</th>
	<td><input id="SYSINFO_NAME" label="系统名称" name="SYSINFO_NAME" type="text" value="<%=pageBean.inputValue("SYSINFO_NAME")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>系统类型</th>
	<td><select id="SYSINFO_TYPE" label="系统类型" name="SYSINFO_TYPE" class="select" ><%=pageBean.selectValue("SYSINFO_TYPE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>IP地址</th>
	<td><textarea id="SYSINFO_IPS" label="IP地址" name="SYSINFO_IPS" cols="70" rows="3" class="textarea"><%=pageBean.inputValue("SYSINFO_IPS")%></textarea><span>可以多个，逗号分隔</span>
</td>
</tr>
<tr>
	<th width="100" nowrap>访问密码</th>
	<td><input id="SYSINFO_PWD" label="访问密码" name="SYSINFO_PWD" type="password" value="<%=pageBean.inputValue("SYSINFO_PWD")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>排序</th>
	<td><input id="SYSINFO_SORT" label="排序" name="SYSINFO_SORT" type="text" value="<%=pageBean.inputValue("SYSINFO_SORT")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><textarea id="SYSINFO_DESC" label="描述" name="SYSINFO_DESC" cols="70" rows="7" class="textarea"><%=pageBean.inputValue("SYSINFO_DESC")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="SYSINFO_ID" name="SYSINFO_ID" value="<%=pageBean.inputValue4DetailOrUpdate("SYSINFO_ID","")%>" />
<input type="hidden" id="SYSINFO_PID" name="SYSINFO_PID" value="<%=pageBean.inputValue("SYSINFO_PID")%>" />
</form>
<script language="javascript">
requiredValidator.add("SYSINFO_CODE");
requiredValidator.add("SYSINFO_NAME");
requiredValidator.add("SYSINFO_TYPE");
requiredValidator.add("SYSINFO_SORT");
intValidator.add("SYSINFO_SORT");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
