<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var operaRequestBox;
function openContentRequestBox(operaType,title,handlerId,subPKField){
	if (('insert' != operaType && !isSelectedRow())||$('#columnId').val() == '00000000-0000-0000-00000000000000000'){
		writeErrorMsg('请先选中有效系统!');
		return;
	}
	if (!operaRequestBox){
		operaRequestBox = new PopupBox('operaRequestBox',title,{size:'big',top:'3px'});
	}
	var columnIdValue = $("#curColumnId").val();
	if ('insert' == operaType){
		columnIdValue = $("#columnId").val();
	}
	var url = 'index?'+handlerId+'&SYSINFO_ID='+columnIdValue+'&operaType='+operaType+'&'+subPKField+'='+$("#"+subPKField).val();
	operaRequestBox.sendRequest(url);
}
function showFilterBox(){
	$('#filterBox').show();
	var clientWidth = $(document.body).width();
	var tuneLeft = (clientWidth - $("#filterBox").width())/2-2;	
	$("#filterBox").css('left',tuneLeft);
}
function isSelectedTree(){
	if (isValid($('#columnId').val())){
		return true;
	}else{
		return false;
	}
}
var operaTreeBox;
function openTreeRequestBox(operaType){
	var title = "树节点管理";
	var handlerId = "SysInfomationEdit";
	
	if ('insert' != operaType && !isSelectedTree()){
		writeErrorMsg('请先选中一个树节点!');
		return;
	}
	if (!operaTreeBox){
		operaTreeBox = new PopupBox('operaTreeBox',title,{size:'normal',width:'500px',height:'360px',top:'3px'});
	}
	var url = '';
	if ('insert' == operaType){
		url = 'index?'+handlerId+'&operaType='+operaType+'&SYSINFO_PID='+$("#columnId").val();
	}else{
		url = 'index?'+handlerId+'&operaType='+operaType+'&SYSINFO_ID='+$("#columnId").val();
	}
	operaTreeBox.sendRequest(url);	
}
function refreshTree(){
	doQuery();
}
function refreshContent(curNodeId){
	if (curNodeId){
		$('#columnId').val(curNodeId);
	}
	doSubmit({actionType:'query'});
}
function deleteTreeNode(){
	if (!isSelectedTree() || $('#columnId').val() == '00000000-0000-0000-00000000000000000'){
		writeErrorMsg('请先选中一个有效系统!');
		return;
	}
	if (confirm('确认要删除该系统吗？')){
		postRequest('form1',{actionType:'deleteTreeNode',onComplete:function(responseText){
			if (responseText == 'success'){
				$('#columnId').val("");
				doQuery();
			}else if (responseText == 'hasChild'){
				writeErrorMsg('该系统还有子节点，不能删除！');
			}else if (responseText == 'hasContent'){
				writeErrorMsg('该系统关联了服务，不能删除！');
			}
		}});	
	}
}
function clearFilter(){
	$("#filterBox input[type!='button'],select").val('');
}

</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;">
	<tr>
	<td valign="top">
    <div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;服务消费方列表 </h3>        
    <div id="treeArea" style="overflow:auto; height:420px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
    <%=pageBean.getStringValue("menuTreeSyntax")%></div>
    </div>
    <b class="b9"></b>
    </div>
    <input type="hidden" id="columnId" name="columnId" value="<%=pageBean.inputValue("columnId")%>" />
    <input type="hidden" id="targetParentId" name="targetParentId" value="" />
    </td>
<td width="85%" valign="top">
<div id="__ToolBar__">
<span style="float:right;height:28px;line-height:28px;">&nbsp;服务类型&nbsp;<select id="serviceType" label="服务类型" name="serviceType" class="select" onchange="doQuery()"><%=pageBean.selectValue("serviceType")%></select></span>
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="openContentRequestBox('insert','服务信息','CallServiceInfomationSelect','SERVICE_ID')"><input value="&nbsp;" title="添加" type="button" class="createImgBtn" style="margin-right:" />添加</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="openContentRequestBox('detail','服务信息','CallServiceInfomationEdit','SERVICE_ID')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="F" align="center" onclick="showFilterBox()"><input value="&nbsp;" title="过滤" type="button" class="filterImgBtn" />过滤</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" hotKey="V" class="bartdx" align="center" onClick="doRequest('viewWSRuntimeStatList')"><input value="&nbsp;" type="button" class="detailImgBtn" title="查看监控" />查看监控</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" hotKey="W" class="bartdx" align="center" onClick="doRequest('viewWebServiceStatChart')"><input value="&nbsp;" type="button" class="relateImgBtn" title="查看统计" />查看统计</td>
</tr>
</table>
</div>
<div id="rightArea">
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="服务信息.csv"
retrieveRowsCallback="process" xlsFileName="服务信息.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="${ec_rd == null ?15:ec_rd}"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();openContentRequestBox('detail','服务信息','ServiceInfomationEdit','SERVICE_ID')" oncontextmenu="selectRow(this,{SERVICE_ID:'${row.SERVICE_ID}',serCode:'${row.SERVICE_CODE}',appName:'${row.SERVICE_PROJECT}',curColumnId:'${row.SYSINFO_ID}'});refreshConextmenu()" onclick="selectRow(this,{SERVICE_ID:'${row.SERVICE_ID}',serCode:'${row.SERVICE_CODE}',appName:'${row.SERVICE_PROJECT}',curColumnId:'${row.SYSINFO_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="SERVICE_PROJECT" title="所属工程"   />
	<ec:column width="100" property="SERVICE_CODE" title="服务编码"   />
	<ec:column width="100" property="SERVICE_NAME" title="服务名称"   />
	<ec:column width="100" property="SERVICE_TYPE" title="服务类型"   />
</ec:row>
</ec:table>

<div id="filterBox" class="sharp color2" style="position:absolute;top:30px;display:none; z-index:10; width:480px;">
<b class="b9"></b>
<div class="content">
<h3>&nbsp;&nbsp;条件过滤框</h3>
<table class="detailTable" cellpadding="0" cellspacing="0" style="width:99%;margin:1px;">
<tr>
	<th width="100" nowrap>服务编码</th>
	<td><input id="serviceCode" label="服务编码" name="serviceCode" type="text" value="<%=pageBean.inputValue("serviceCode")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>服务名称</th>
	<td><input id="serviceName" label="服务名称" name="serviceName" type="text" value="<%=pageBean.inputValue("serviceName")%>" size="24" class="text" />
</td>
</tr>
</table>
<div style="width:100%;text-align:center;">
<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
&nbsp;&nbsp;
<input type="button" name="button" id="button" value="清空" class="formbutton" onclick="clearFilter()" />
&nbsp;&nbsp;<input type="button" name="button" id="button" value="关闭" class="formbutton" onclick="javascript:$('#filterBox').hide();" /></div>
</div>
<b class="b9"></b>
</div>
<input type="hidden" id="_tabId_" name="_tabId_" value="<%=pageBean.inputValue("_tabId_")%>" />
<input type="hidden" name="SERVICE_ID" id="SERVICE_ID" value="" />
<input type="hidden" name="serCode" id="serCode" value="" />
<input type="hidden" name="appName" id="appName" value="" />
<input type="hidden" name="curColumnId" id="curColumnId" value="" />
<script language="JavaScript">
setRsIdTag('SERVICE_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
$(function(){
	resetTreeHeight(79);
	resetRightAreaHeight(87);
});
</script>
</div>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
