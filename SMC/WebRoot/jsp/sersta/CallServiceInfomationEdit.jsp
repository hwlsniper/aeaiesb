<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String localAddr = request.getLocalAddr();
if ("0.0.0.0".equals(localAddr)){
	localAddr = "localhost";
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if (responseText == 'success'){
			parent.refreshContent();
			parent.PopupBox.closeCurrent();
		}
	}});	
}
var invokeBox;
function showInvokeBoxRequest(){
	if (!invokeBox){
		invokeBox = new PopupBox('invokeBox','手工调用',{size:'normal',width:'500px',height:'360px',top:'3px'});
	}
	var url = 'index?HttpFlowInvoke&mfId=<%=pageBean.inputValue("SERVICE_ID")%>&appId=<%=pageBean.inputValue("APP_ID")%>';
	invokeBox.sendRequest(url);
}
function showBzilogConfig(){
	var value = $('#ENABLE_BIZLOG option:selected') .val();
	if("N" == value){
		$("#bzilogConfig").css('display','none');
	}else if("Y" == value){
		$("#bzilogConfig").css('display','');
	}
}
$(document).ready(function() {
	var logValue = $('#ENABLE_BIZLOG option:selected') .val();
	if("N" == logValue){
		$("#bzilogConfig").css('display','none');
	}else if("Y" == logValue){
		$("#bzilogConfig").css('display','');
	}
});
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>服务名称</th>
	<td><input id="SERVICE_NAME" label="服务名称" name="SERVICE_NAME" type="text" value="<%=pageBean.inputValue("SERVICE_NAME")%>" size="65" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>服务编码</th>
	<td><input id="SERVICE_CODE" label="服务编码" name="SERVICE_CODE" type="text" value="<%=pageBean.inputValue("SERVICE_CODE")%>" size="65" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>服务类型</th>
	<td><input id="SERVICE_TYPE" label="服务类型" name="SERVICE_TYPE" type="text" value="<%=pageBean.inputValue("SERVICE_TYPE")%>" size="65" class="text" />
</td>
</tr>
<%if ("Web Service".equalsIgnoreCase(pageBean.inputValue("SERVICE_TYPE"))){%>
<tr>
	<th width="100" nowrap>服务URL</th>
	<td><input id="WS_ADDRESS" label="服务URL" name="WS_ADDRESS" type="text" value="http://<%=localAddr%>:<%=request.getLocalPort()%>/<%=pageBean.inputValue("SERVICE_PROJECT")%>/services/<%=pageBean.inputValue("SERVICE_CODE")%>?wsdl" size="65" class="text editable" />
</td>
</tr>
<%}%>
<%if ("Web Service Proxy".equalsIgnoreCase(pageBean.inputValue("SERVICE_TYPE"))){%>
<tr>
	<th width="100" nowrap>服务URL</th>
	<td><input id="WS_ADDRESS" label="服务URL" name="WS_ADDRESS" type="text" value="<%=pageBean.inputValue("WS_ADDRESS")%>" size="65" class="text editable" />
</td>
</tr>
<tr>
	<th width="100" nowrap>代理URL</th>
	<td><input id="WS_PROXY" label="代理URL" name="WS_PROXY" type="text" value="http://<%=localAddr%>:<%=request.getLocalPort()%>/<%=pageBean.inputValue("SERVICE_PROJECT")%>/wsproxies/<%=pageBean.inputValue("SERVICE_CODE")%>?wsdl" size="65" class="text sign" readonly="readonly" />
</td>
</tr>
<%}%>
<tr>
	<th width="100" nowrap>业务日志</th>
	<td><select id="ENABLE_BIZLOG" label="业务日志" name="ENABLE_BIZLOG" class="select" onchange="showBzilogConfig()"><%=pageBean.selectValue("ENABLE_BIZLOG")%></select>
</td>
</tr>
<tr id="bzilogConfig">
	<th width="100" nowrap >日志流程</th>
	<td><input id="BZILOG_CONFIG" label="日志流程" name="BZILOG_CONFIG" type="text" value="<%=pageBean.inputValue("BZILOG_CONFIG")%>" size="65" class="text editable" />
</td>
</tr>
<tr>
	<th width="100" nowrap>服务描述</th>
	<td><textarea id="SERVICE_DESC" label="服务描述" name="SERVICE_DESC" cols="75" rows="3" class="textarea"><%=pageBean.inputValue("SERVICE_DESC")%></textarea>
</td>
</tr>
</table>
<table width="100%" border="0">
  <tr>
    <td align="center"><input type="button" name="button" id="button" value="关闭窗口" class="formbutton" onClick="parent.PopupBox.closeCurrent()"></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" name="SYSINFO_ID" id="SYSINFO_ID" value="<%=pageBean.inputValue("SYSINFO_ID")%>"/>
<input type="hidden" id="SERVICE_ID" name="SERVICE_ID" value="<%=pageBean.inputValue4DetailOrUpdate("SERVICE_ID","")%>" />
<input type="hidden" name="APP_ID" id="APP_ID" value="<%=pageBean.inputValue("APP_ID")%>"/>
</form>
<script language="javascript">
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
