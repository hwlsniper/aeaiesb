<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveTreeRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if ('success' == responseText){
			parent.refreshTree();
			parent.PopupBox.closeCurrent();
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveTreeRecord()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>系统</th>
	<td><input id="SYSINFO_CODE" label="系统" name="SYSINFO_CODE" type="text" value="<%=pageBean.inputValue("SYSINFO_CODE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>SYSINFO_NAME</th>
	<td><input id="SYSINFO_NAME" label="SYSINFO_NAME" name="SYSINFO_NAME" type="text" value="<%=pageBean.inputValue("SYSINFO_NAME")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>SYSINFO_PWD</th>
	<td><input id="SYSINFO_PWD" label="SYSINFO_PWD" name="SYSINFO_PWD" type="text" value="<%=pageBean.inputValue("SYSINFO_PWD")%>" size="32" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>SYSINFO_DESC</th>
	<td><input id="SYSINFO_DESC" label="SYSINFO_DESC" name="SYSINFO_DESC" type="text" value="<%=pageBean.inputValue("SYSINFO_DESC")%>" size="65" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="SYSINFO_ID" name="SYSINFO_ID" value="<%=pageBean.inputValue4DetailOrUpdate("SYSINFO_ID","")%>" />
<input type="hidden" id="SYSINFO_SORT" name="SYSINFO_SORT" value="<%=pageBean.inputValue("SYSINFO_SORT")%>" />
<input type="hidden" id="SYSINFO_PID" name="SYSINFO_PID" value="<%=pageBean.inputValue("SYSINFO_PID")%>" />
</form>
<script language="javascript">
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
