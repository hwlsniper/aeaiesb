<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>服务信息选择</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function setSelectTempValue(idValue,nameValue){
	ele('targetIdValue').value = idValue;
	ele('targetNameValue').value = nameValue;
}
function doSelectRequest(){
	bachProcessIds();
	var ids = $("#ids").val();
	if (ids == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	postRequest('form1',{actionType:'addService',onComplete:function(responseText){
		if (responseText == 'success'){
			parent.PopupBox.closeCurrent();
			parent.doQuery();
		}
	}});
}
function bachProcessIds(){
	var ids = "";
	$("input:[name='SERVICE_ID'][checked]").each(function(){   
		ids = ids+$(this).val()+",";
	});
	if (ids.length > 0){
		ids = ids.substring(0,ids.length-1);
	}
	$('#ids').val(ids);
}
function showBzilogConfig(){
	var value = $('#ENABLE_BIZLOG option:selected') .val();
	if("N" == value){
		$("#bzilogConfig").css('display','none');
	}else if("Y" == value){
		$("#bzilogConfig").css('display','');
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doSelectRequest()"><input value="&nbsp;" type="button" class="saveImgBtn" title="选择" />选择</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="B" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>        
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable">
<tr><td>
&nbsp;所属工程<select id="appName" label="所属工程" name="appName" class="select" onchange="doQuery()"><%=pageBean.selectValue("appName")%></select>
&nbsp;服务编码<input id="serviceCode" label="服务编码" name="serviceCode" type="text" value="<%=pageBean.inputValue("serviceCode")%>" size="15" class="text" />
&nbsp;服务名称<input id="serviceName" label="服务名称" name="serviceName" type="text" value="<%=pageBean.inputValue("serviceName")%>" size="15" class="text" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="服务信息选择.csv"
retrieveRowsCallback="process" xlsFileName="服务信息选择.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="10"
listWidth="100%" 
height="auto" 
>
<ec:row styleClass="odd" ondblclick="clearSelection();" onclick="setSelectTempValue('${row.SERVICE_ID}','${row.SERVICE_NAME}')">
	<ec:column width="20" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="25" style="text-align:center" property="SERVICE_ID" cell="checkbox" headerCell="checkbox" onclick="$(this).parents('tr').trigger('click');"/>
	<ec:column width="100" property="SERVICE_PROJECT" title="所属工程"   />
	<ec:column width="100" property="SERVICE_CODE" title="服务编码"   />
	<ec:column width="100" property="SERVICE_NAME" title="服务名称"   />
	<ec:column width="100" property="SERVICE_TYPE" title="服务类型"   />
</ec:row>
</ec:table>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="targetId" id="targetId" value="<%=pageBean.inputValue("targetId")%>" />
<input type="hidden" name="targetName" id="targetName" value="<%=pageBean.inputValue("targetName")%>" />
<input type="hidden" name="targetIdValue" id="targetIdValue" value="" />
<input type="hidden" name="targetNameValue" id="targetNameValue" value="" />
<input type="hidden" name="SYSINFO_ID" id="SYSINFO_ID" value="<%=pageBean.inputValue("SYSINFO_ID")%>" />
<input type="hidden" name="ids" id="ids" value="<%=pageBean.inputValue("ids")%>" />
<script language="JavaScript">
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
