<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>服务代理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function reloadAll(){
	postRequest('form1',{actionType:'reloadAll',onComplete:function(responseText){
		if (responseText == 'success'){
			alert("重新加载成功！");
		}else{
			alert('加载缓存失败！！');
		}
	}});
}

function doDeleteReq(){	
	var ids = "";
	var confirmsubMsg="您确认删除该数据吗？";
	$("input:[name = 'WS_ID'][checked]").each(function(){ 
		ids = ids+$(this).val()+",";
	});
	if (ids.length > 0){
		ids = ids.substring(0,ids.length-1);
	}
	if (ids == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	if(confirm(confirmsubMsg)) {
		$("#ids").val(ids);
		doSubmit({actionType:'batchDelete'});
	}
}

function selectedCheck(indexId){	
	var idInt = parseInt(indexId);
	var currentIndexId = idInt ;
	if($("#ec_table tr:eq("+currentIndexId+") input[name = 'WS_ID']").is(':checked')){
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'WS_ID']").attr('checked',false);
	}else{
		$("#ec_table tr:eq("+currentIndexId+") input[name = 'WS_ID']").attr('checked',true);
	}
}

function bachProcessIds(){
	var ids = "";
	$("input:[name = 'WS_ID'][checked]").each(function(){   
		ids = ids+$(this).val()+",";
	});
	if (ids.length > 0){
		ids = ids.substring(0,ids.length-1);
	}
	$("#ids").val(ids);
}

function showDetail(actionType,webServiceId) {
	var ids = '';
	if(!webServiceId){
		bachProcessIds();
		ids = $("#ids").val();
		if(ids.length > 36){
			writeErrorMsg('只能选中一条记录!');
			return;
		}
		if (ids == ""){
			writeErrorMsg('请先选中一条记录!');
			return;
		}
	}else{
		ids = webServiceId;
	}
	
	var appId = $("#APP_ID").val();
	var url = "/index?WsProxyManageEdit&operaType=detail&WS_ID="+ids+"&APP_ID="+appId;
	var path = "<%=request.getContextPath() %>";
	url = path + url;
	window.location.href = url;
}

function copyRecord(actionType) {
	bachProcessIds();
	var ids = $("#ids").val();
	if(ids.length > 36){
		writeErrorMsg('只能选中一条记录!');
		return;
	}
	if (ids == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	var appId = $("#APP_ID").val();
	var url = "/index?WsProxyManageEdit&operaType=copy&WS_ID="+ids+"&APP_ID="+appId;
	var path = "<%=request.getContextPath() %>";
	url = path + url;
	window.location.href = url;
}

function showUpdate(actionType) {
	bachProcessIds();
	var ids = $("#ids").val();
	if(ids.length > 36){
		writeErrorMsg('只能选中一条记录!');
		return;
	}
	if (ids == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}
	var appId = $("#APP_ID").val();
	var url = "/index?WsProxyManageEdit&operaType=update&WS_ID="+ids+"&APP_ID="+appId;
	var path = "<%=request.getContextPath() %>";
	url = path + url;
	window.location.href = url;
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="showUpdate('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="copyRecord('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="showDetail('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="reloadAll()"><input value="&nbsp;" type="button" class="refreshImgBtn" id="refreshImgBtn" title="重新加载" />重新加载</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDeleteReq();"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="服务代理.csv"
retrieveRowsCallback="process" xlsFileName="服务代理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();showDetail('viewDetail','${row.WS_ID}')" oncontextmenu="selectRow(this,{WS_ID:'${row.WS_ID}'});refreshConextmenu()" onclick="selectedCheck('${GLOBALROWCOUNT}');selectRow(this,{WS_ID:'${row.WS_ID}'})">
	<ec:column width="25" style="text-align:center" property="WS_ID" cell="checkbox" headerCell="checkbox" />
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="WS_NAME" title="服务名称"   />
	<ec:column width="100" property="WS_ALIAS" title="服务别名"   />
	<ec:column width="100" property="WS_ADDRESS" title="地址URL"   />
	<ec:column width="100" property="WS_STATE" title="是否启用"   mappingItem="WS_STATE"/>
	<ec:column width="100" property="ENABLE_AUTH" title="认证类型"   mappingItem="ENABLE_AUTH"/>
	<ec:column width="100" property="ENABLE_BIZLOG" title="业务日志"   mappingItem="ENABLE_BIZLOG"/>
</ec:row>
</ec:table>
<input type="hidden" name="WS_ID" id="WS_ID" value="" />
<input type="hidden" name="APP_ID" id="APP_ID" value="<%=pageBean.inputValue("APP_ID")%>" />
<input type="hidden" id="ids" name="ids" value="<%=pageBean.inputValue("ids")%>" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('WS_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
