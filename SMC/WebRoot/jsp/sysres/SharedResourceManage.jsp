<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>共享资源</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function changeTab(tabId){
	if("mailFrame" == tabId){
		var url = "index?MailResourceManageList";
		$('#mailFrame').attr("src",url);  
	}else if("ftpFrame" == tabId){
		var url = "index?FtpResourceManageList";
		$('#ftpFrame').attr("src",url);  
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div style="padding-top: 7px;">
<div class="photobg1" id="tabHeader">
    <div class="newarticle1" onclick="changeTab('ftpFrame')">FTP资源</div>
    <div class="newarticle1" onclick="changeTab('mailFrame')">Mail资源</div>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:540px;display:none;overflow:hidden;">
<iframe id="ftpFrame" src="" width="100%" height="530" frameborder="0" scrolling="no"></iframe>
</div>
<div class="photobox newarticlebox" id="Layer1" style="height:540px;display:none;overflow:hidden;">
<iframe id="mailFrame" src="" width="100%" height="530" frameborder="0" scrolling="no"></iframe>
</div>
</div>
</form>
<script language="JavaScript">
$(function(){
	changeTab('ftpFrame');
	resetTabHeight(80);
});
var tab = new Tab('tab','tabHeader','Layer',0);
tab.focus(0);
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
