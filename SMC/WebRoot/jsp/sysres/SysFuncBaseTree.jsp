<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
	function doSave() {
		fillIds();
		if (!isValid(ele('FUNC_IDS').value)) {
			writeErrorMsg('请先选中一条记录!');
			return;
		}
		
		var action = "index?SysUserManageList&actionType=saveUserAuth";
		$("#form1").attr('action', action);
		postRequest('form1', {onComplete:function(responeText){
			if(responeText != 'true') {
				alert('保存失败!');
			} else {
				parent.PopupBox.closeCurrent();
			}
		}});
	}
	
	var tree;
	function fillIds() {
		var chk = tree.Nodes;
		var tempId = '';
		for (var o in chk) {
			if(chk[o].checked){
				tempId += chk[o].ID + ',';
			}
		}

		if (tempId.length > 0) {
			$('#FUNC_IDS').val(tempId);
		}
	}

	function setSelectedOpt() {
		var checkedIds = '<%=pageBean.inputValue("checkedIds")%>';
		var checkedArray = checkedIds.split(',');
		for (var i = 0; i < checkedArray.length; i++) {
			$("#checkbox" + checkedArray[i]).attr("checked",'true');
			tree.Nodes[checkedArray[i]].checked = true;
		}
	}
</script>
</head>
<body onload="setSelectedOpt();">
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<table class="detailTable" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" nowrap>
<div id="selectTreeContainer" style="height:312px;overflow-y:auto" ondblclick="doSelectRequest()">
<script language="javascript">
<%=pageBean.inputValue("pickTreeSyntax")%>
</script>
</div>	
	</td>
  </tr>
  <tr>
    <td align="center">
    <input class="formbutton" type="button" name="Button23" value="保存" onclick="doSave()"/>&nbsp; &nbsp;
    <input class="formbutton" type="button" name="Button22" value="关闭" onclick="javascript:parent.PopupBox.closeCurrent();"/></td>
  </tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="targetId" id="targetId" value="FUNC_PID" />
<input type="hidden" name="FUNC_IDS" id="FUNC_IDS" value="" />
<input type="hidden" name="AUTH_USER_ID" id="AUTH_USER_ID" value="<%=pageBean.inputValue("AUTH_USER_ID")%>" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
