<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Mail资源</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save',checkUnique:'true'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>资源编码</th>
	<td><input id="RES_CODE" label="资源编码" name="RES_CODE" type="text" value="<%=pageBean.inputValue("RES_CODE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>资源名称</th>
	<td><input id="RES_NAME" label="资源名称" name="RES_NAME" type="text" value="<%=pageBean.inputValue("RES_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>服务器地址</th>
	<td><input id="RES_IP" label="服务器地址" name="RES_IP" type="text" value="<%=pageBean.inputValue("RES_IP")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>服务器端口</th>
	<td><input id="RES_PORT" label="服务器端口" name="RES_PORT" type="text" value="<%=pageBean.inputValue("RES_PORT")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>邮箱地址</th>
	<td><input id="RES_ADSS" label="邮箱地址" name="RES_ADSS" type="text" value="<%=pageBean.inputValue("RES_ADSS")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>用户名称</th>
	<td><input id="RES_USERNAME" label="用户名称" name="RES_USERNAME" type="text" value="<%=pageBean.inputValue("RES_USERNAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>用户密码</th>
	<td><input id="RES_PASS" label="用户密码" name="RES_PASS" type="password" value="<%=pageBean.inputValue("RES_PASS")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>资源描述</th>
	<td><textarea id="RES_DESC" label="资源描述" name="RES_DESC" cols="40" rows="3" class="textarea"><%=pageBean.inputValue("RES_DESC")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="RES_ID" name="RES_ID" value="<%=pageBean.inputValue4DetailOrUpdate("RES_ID","")%>" />
</form>
<script language="javascript">
requiredValidator.add("RES_CODE");
requiredValidator.add("RES_NAME");
requiredValidator.add("RES_IP");
requiredValidator.add("RES_PORT");
requiredValidator.add("RES_ADSS");
intValidator.add("RES_PORT");
emailValidator.add("RES_ADSS");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
