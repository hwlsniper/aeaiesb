<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String scheme = request.getScheme();
String localAddr = request.getServerName();
if ("0.0.0.0".equals(localAddr)){
	localAddr = "localhost";
}
int serverPort = request.getServerPort();
String urlPrefix = scheme+"://"+localAddr;
if (serverPort != 80){
	urlPrefix = urlPrefix+":"+serverPort;
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>服务代理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function showAuthConfig(){
	var value = $('#ENABLE_AUTH option:selected') .val();
	if("N" == value){
		$("#authConfig").css('display','none');
	}else if("U" == value){
		$("#authConfig").css('display','');
		var authConfig ='{"authType":"userId8password","userId":"admin","password":"admin"}';
		$("#AUTH_CONFIG").val(authConfig);
	}else if("P" == value){
		$("#authConfig").css('display','');
		var appName = $("#APP_NAME").val();
		var authConfig ='{"authType":"basicFlow","appName":"'+appName+'","flowName":"changeIt"}';
		$("#AUTH_CONFIG").val(authConfig);
	}else if("X" == value){
		$("#authConfig").css('display','none');
		var authConfig ='{"authType":"consumerWhiteIpList"}';
		$("#AUTH_CONFIG").val(authConfig);
	}else if("Y" == value){
		$("#authConfig").css('display','none');
		var authConfig ='{"authType":"consumerAppToken"}';
		$("#AUTH_CONFIG").val(authConfig);
	}else if("Z" == value){
		$("#authConfig").css('display','none');
		var authConfig ='{"authType":"consumerMiscAuth"}';
		$("#AUTH_CONFIG").val(authConfig);
	}
}

function showBzilogConfig(){
	var value = $('#ENABLE_BIZLOG option:selected') .val();
	if("N" == value){
		$("#bzilogConfig").css('display','none');
	}else if("Y" == value){
		$("#bzilogConfig").css('display','');
	}
}

function analysisWsName8Accessuri(){
	postRequest('form1',{actionType:'analysisWsName8Accessuri',onComplete:function(responseText){
		if (responseText == 'fail'){
			writeErrorMsg("解析服务名称,访问地址失败,请检查服务URL!!!");
			hideSplash();
		}else{
			var json = jQuery.parseJSON(responseText);
			var wsName = json.servicName;
			var wsAccessuri = json.locationURI;
			$('#WS_NAME').val(wsName);
			$('#WS_ALIAS').val(wsName);
			$('#WS_ACCESSURI').val(wsAccessuri);
		}
	}});
}

$(document).ready(function() {
	var value = $('#ENABLE_AUTH option:selected') .val();
	if("N" == value || "X" == value || "Y" == value || "Z" == value){
		$("#authConfig").css('display','none');
	}else if("U" == value){
		$("#authConfig").css('display','');
	}else if("P" == value){
		$("#authConfig").css('display','');
	}
	
	var logValue = $('#ENABLE_BIZLOG option:selected') .val();
	if("N" == logValue){
		$("#bzilogConfig").css('display','none');
	}else if("Y" == value){
		$("#bzilogConfig").css('display','');
	}
});

function goToList() {
	var appId = $("#APP_ID").val();
	var url = "/index?WsProxyManageList&APP_ID="+appId;
	var path = "<%=request.getContextPath() %>";
	url = path + url;
	window.location.href = url;
}

</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save',checkUnique:'true'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToList();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>服务名称</th>
	<td colspan="3"><input id="WS_NAME" label="服务名称" name="WS_NAME" type="text" value="<%=pageBean.inputValue("WS_NAME")%>" size="24" class="text" readonly="readonly" />
</td>
</tr>
<tr>
	<th width="100" nowrap>服务别名</th>
	<td colspan="3"><input id="WS_ALIAS" label="服务别名" name="WS_ALIAS" type="text" value="<%=pageBean.inputValue("WS_ALIAS")%>" size="24" class="text editable" />
</td>
</tr>
<tr>
	<th width="100" nowrap>服务URL</th>
	<td colspan="3"><input id="WS_ADDRESS" label="服务URL" name="WS_ADDRESS" type="text" value="<%=pageBean.inputValue("WS_ADDRESS")%>" size="66" class="text editable" />
	<input type="button" class="formbutton" name="analysisButton" id="analysisButton" value="解析" onclick="analysisWsName8Accessuri()" />
</td>
</tr>
<%if("Y".equals(pageBean.getAttribute("showWsProxy"))){ %>
<tr>
	<th width="100" nowrap>代理URL</th>
	<td colspan="3"><input id="WS_PROXY" label="代理URL" name="WS_PROXY" type="text" value="<%=urlPrefix%>/<%=pageBean.inputValue("APP_NAME")%>/wsproxies/<%=pageBean.inputValue("WS_NAME")%>?wsdl" size="66" class="text sign" readonly="readonly" />
</td>
</tr>
<%} %>
<tr>
	<th width="100" nowrap>超时时间</th>
	<td width="40%"><input id="WS_TIMEOUT" label="超时时间" name="WS_TIMEOUT" type="text" value="<%=pageBean.inputValue("WS_TIMEOUT")%>" size="24" class="text editable" /> 秒
</td>
	<th width="100" nowrap>字符集</th>
	<td width="40%"><input id="WS_CHARSET" label="字符集" name="WS_CHARSET" type="text" value="<%=pageBean.inputValue("WS_CHARSET")%>" size="24" class="text editable" />
	  如：UTF-8、GBK，默认自适应 </td>
</tr>
<tr>
  <th width="100" nowrap>创建人</th>
  <td><input id="CREATE_USER_NAME" label="创建人" name="CREATE_USER_NAME" type="text" value="<%=pageBean.inputValue("CREATE_USER_NAME")%>" size="24" class="text" readonly="readonly" />
    <input type="hidden" name="CREATE_USER" id="CREATE_USER" value="<%=pageBean.inputValue("CREATE_USER")%>"/>
  </td>
  <th width="100" nowrap>创建时间</th>
  <td><input id="CREATE_TIME" label="创建时间" name="CREATE_TIME" type="text" value="<%=pageBean.inputTime("CREATE_TIME")%>"  readonly="readonly" size="24" class="text" /></td>
</tr>
 <% if(pageBean.getBoolValue("showUpdateUser")){%>
<tr>
	<th width="100" nowrap>更新人</th>
	<td><input id="UPDATE_USER_NAME" label="更新人" name="UPDATE_USER_NAME" type="text" value="<%=pageBean.inputValue("UPDATE_USER_NAME")%>" size="24" class="text" readonly="readonly" />
	  <input type="hidden" name="UPDATE_USER" id="UPDATE_USER" value="<%=pageBean.inputValue("UPDATE_USER")%>"/></td>
	<th width="100" nowrap>更新时间</th>
	<td><input id="UPDATE_TIME" label="更新时间" name="UPDATE_TIME" type="text" value="<%=pageBean.inputTime("UPDATE_TIME")%>" size="24" class="text" readonly="readonly" /></td>
</tr>
<%}%>
<tr>
	<th width="100" nowrap>是否启用</th>
	<td colspan="3"><%=pageBean.selectRadio("WS_STATE")%>
</td>
</tr>
<tr>
	<th width="100" nowrap>认证类型</th>
	<td colspan="3"><select id="ENABLE_AUTH" label="认证类型" name="ENABLE_AUTH" class="select" onchange="showAuthConfig()" style="width:220px"><%=pageBean.selectValue("ENABLE_AUTH")%></select>
</td>
</tr>
<tr id="authConfig">
	<th width="100" nowrap>认证配置</th>
	<td colspan="3"><textarea id="AUTH_CONFIG" label="认证配置" name="AUTH_CONFIG" cols="75" rows="3" class="text editable" style="width:600px"><%=pageBean.inputValue("AUTH_CONFIG")%></textarea>
</td>
</tr>
<tr>
	<th width="100" nowrap>业务日志</th>
	<td colspan="3"><select id="ENABLE_BIZLOG" label="业务日志" name="ENABLE_BIZLOG" class="select" onchange="showBzilogConfig()" style="width:220px"><%=pageBean.selectValue("ENABLE_BIZLOG")%></select>
</td>
</tr>
<tr id="bzilogConfig">
	<th width="100" nowrap >日志流程</th>
	<td colspan="3"><input id="BZILOG_CONFIG" label="日志流程" name="BZILOG_CONFIG" type="text" value="<%=pageBean.inputValue("BZILOG_CONFIG")%>" size="66" class="text editable" style="width:600px" />
</td>
</tr>
<tr>
	<th width="100" nowrap>生产者</th>
	<td colspan="3"><input id="PRODUCER_NAME" label="生产者" name="PRODUCER_NAME" type="text" value="<%=pageBean.inputValue("PRODUCER_NAME")%>" size="24" class="text" readonly="readonly" style="width:600px" /></td>
	</tr>
<tr>
	<th width="100" nowrap>消费者</th>
	<td colspan="3"><input id="CONSUMER_NAME" label="消费者" name="CONSUMER_NAME" type="text" value="<%=pageBean.inputTime("CONSUMER_NAME")%>" size="24" class="text" readonly="readonly" style="width:600px" /></td>
</tr>
<tr>
	<th width="100" nowrap>服务描述</th>
	<td colspan="3"><textarea id="WS_DESC" label="服务描述" name="WS_DESC" cols="75" rows="3" class="text editable" style="width:600px"><%=pageBean.inputValue("WS_DESC")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="WS_ID" name="WS_ID" value="<%=pageBean.inputValue("WS_ID")%>" />
<input type="hidden" id="APP_ID" name="APP_ID" value="<%=pageBean.inputValue("APP_ID")%>" />
<input type="hidden" id="APP_NAME" name="APP_NAME" value="<%=pageBean.inputValue("APP_NAME")%>" />
<input id="WS_ACCESSURI" label="访问地址" name="WS_ACCESSURI" type="hidden" value="<%=pageBean.inputValue("WS_ACCESSURI")%>" />

</form>
<script language="javascript">
requiredValidator.add("WS_NAME");
requiredValidator.add("WS_ADDRESS");
intValidator.add("WS_TIMEOUT");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
