<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>数据资源管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function refreshInfoFields(){
	postRequest('form1',{actionType:'refreshInfoFields',onComplete:function(responseText){
		var object = jQuery.parseJSON(responseText);
		var dbURL = object.dbURL;
		var dbDriver = object.dbDriver;
		$("#DB_URL").val(dbURL);
		$("#DB_DRIVER").val(dbDriver);
	}});
}
function testConnection(){
	postRequest('form1',{actionType:'testConnection',onComplete:function(responseText){
		if (responseText == 'success'){
			alert('连接成功！');
		}else{
			alert('连接失败，请检查配置！');
		}
	}});	
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="testConnection()"><input value="&nbsp;" type="button" class="relateImgBtn" id="saveImgBtn" title="测试连接" />测试连接</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="DB_NAME" label="名称" name="DB_NAME" type="text" value="<%=pageBean.inputValue("DB_NAME")%>" size="42" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="DB_ALIAS" label="别名" name="DB_ALIAS" type="text" value="<%=pageBean.inputValue("DB_ALIAS")%>" size="42" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>模板</th>
	<td><select style="width:200px;" name="DB_TMP_ID" id="DB_TMP_ID" onchange="refreshInfoFields();"><%=pageBean.selectValue("DB_TMP_ID") %>
	  </select></td>
</tr>
<tr>
	<th width="100" nowrap>连接URL</th>
	<td><input id="DB_URL" label="链接URL" name="DB_URL" type="text" value="<%=pageBean.inputValue("DB_URL")%>" size="42" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>连接驱动</th>
	<td><input id="DB_DRIVER" label="驱动" name="DB_DRIVER" type="text" value="<%=pageBean.inputValue("DB_DRIVER")%>" size="42" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>用户名</th>
	<td><input id="DB_USER" label="用户名" name="DB_USER" type="text" value="<%=pageBean.inputValue("DB_USER")%>" size="42" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>密码</th>
	<td><input id="DB_PWD" label="密码" name="DB_PWD" type="password" value="<%=pageBean.inputValue("DB_PWD")%>" size="42" class="text" /></td>
</tr>
<tr>
	<th width="100" nowrap>测试SQL</th>
	<td><input id="DB_TESTSQL" label="测试SQL" name="DB_TESTSQL" type="text" value="<%=pageBean.inputValue("DB_TESTSQL")%>" size="42" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>最大连接数</th>
	<td><input id="DB_MAXCONN" label="最大连接数" name="DB_MAXCONN" type="text" value="<%=pageBean.inputValue("DB_MAXCONN")%>" size="42" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>空闲连接数</th>
	<td><input id="DB_IDLECONN" label="空闲连接数" name="DB_IDLECONN" type="text" value="<%=pageBean.inputValue("DB_IDLECONN")%>" size="42" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>扩展属性</th>
	<td><input id="DB_PROPERTIES" label="扩展属性" name="DB_PROPERTIES" type="text" value="<%=pageBean.inputValue("DB_PROPERTIES")%>" size="42" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><input id="DB_DESC" label="描述" name="DB_DESC" type="text" value="<%=pageBean.inputValue("DB_DESC")%>" size="42" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="DB_ID" name="DB_ID" value="<%=pageBean.inputValue("DB_ID")%>" />
</form>
<script language="javascript">
requiredValidator.add("DB_NAME");
requiredValidator.add("DB_ALIAS");
requiredValidator.add("DB_URL");
requiredValidator.add("DB_DRIVER");
requiredValidator.add("DB_USER");
requiredValidator.add("DB_PWD");
requiredValidator.add("DB_MAXCONN");
intValidator.add("DB_MAXCONN");
requiredValidator.add("DB_IDLECONN");
intValidator.add("DB_IDLECONN");
initDetailOpertionImage();
<% if (pageBean.isOnCreateMode()){ %>
refreshInfoFields();
<%}%>
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
