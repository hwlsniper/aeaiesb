<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>运行状态监控</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link rel="stylesheet" href="css/icon.css" type="text/css" />
<link rel="stylesheet" href="css/easyui.css" type="text/css" />	
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery.easyui.min.js"></script>	
<script type="text/javascript" src="js/jquery.pagination.js"></script>
<script type="text/javascript">
var errorBox;
function showErrorBox(appName,messageFlowId){
	//clearSelection();
	if (!errorBox){
		errorBox = new PopupBox('errorBox','错误信息查看',{size:'big',height:'550px',top:'30px'});
	}
	var url = "index?MFRuntimeErrors&appName="+appName+"&messageFlowId="+messageFlowId;
	errorBox.sendRequest(url);
}

var defaultRange = 5;
var leftRange = defaultRange;

var intervalId;

function immedateRefresh(){
	leftRange = leftRange-1;
	if (leftRange == 0){
		$("#tipText").val('正在执行刷新操作！');
		leftRange = defaultRange;
		$('#dg').datagrid('reload');
	}
	$("#tipText").html(leftRange+'秒钟后进行刷新操作...');
}

function trigerRefresh(){
	var trigerRefreshBtn = $("#trigerRefreshBtn");
	if (trigerRefreshBtn.val() == '停止自动刷新'){
		clearInterval(intervalId);
		trigerRefreshBtn.val('启动自动刷新');
	}
	else{
		intervalId = setInterval(immedateRefresh,1000);
		trigerRefreshBtn.val('停止自动刷新');
	}
}

function resetAutoRefresh(){
	defaultRange = $("#timerRangeList").val();
	leftRange = defaultRange;
	
	var trigerRefreshBtn = $("#trigerRefreshBtn");
	if (trigerRefreshBtn.val() == '停止自动刷新'){
		clearInterval(intervalId);
		intervalId = setInterval(immedateRefresh,1000);
	}
}

function resetMonitor(){
	sendRequest("<%=pageBean.getHandlerURL()%>&actionType=resetMonitor",{dataType:"text",onComplete:function(responseText){
		immedateRefresh();
	}});	
}

$(function(){
	$("#timerRangeList").val(defaultRange);
	intervalId = setInterval(immedateRefresh,1000);
}); 

function frameHeight(){  
	var height = $(window).height() - 50;
	return height;
}
function frameWidth(){  
	var width = $(window).width() - 10;
	return width;
}  
$(document).ready(function() {
	$('#dg').datagrid({  
        width:'auto', 
        height:frameHeight(),
        //striped: true,  
        singleSelect : true, 
        url:'<%=pageBean.getHandlerURL()%>&actionType=loadMonitorData&appName='+$("#appName").val(), 
        //queryParams:{},  
        idField:'messageFlowId',
        loadMsg:'数据加载中请稍后……',  
        pagination:true,//显示分页  
	   	rownumbers:true,//行号 
	   	//fit:true,//自动补全  
        fitColumns:true,
        columns:[[
      	        {field:'appName',title:'应用',width:100},
      	        {field:'messageFlowName',title:'名称',width:100},
      	        {field:'messageFlowAlias',title:'别名',width:100,align:'right'},
      	        {field:'messageFlowType',title:'流程类型',width:100},
      	        {field:'totalCount',title:'运行次数',width:100,align:'right'},
      	        {field:'successCount',title:'成功次数',width:100,align:'right'},
      	        {field:'failureCount',title:'失败次数',width:100,align:'right'},
      	        {field:'maxRuntime',title:'最长时间',width:100,align:'right'},
      	        {field:'minRuntime',title:'最短时间',width:100,align:'right'},
      	        {field:'averageRuntime',title:'平均时间',width:100,align:'right'}
      	    ]],
      	onDblClickRow: function (index,row) {//双击的逻辑和单机的那个差不多  
              showErrorBox(row.appName,row.messageFlowId);
          },  
    });
	 var p = $('#dg').datagrid('getPager'); 
	    $(p).pagination({ 
	        pageSize: 10,//每页显示的记录条数，默认为10 
	        pageList: [5,10,15],//可以设置每页记录条数的列表 
	        beforePageText: '第',//页数文本框前显示的汉字 
	        afterPageText: '页    共 {pages} 页', 
	        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录', 
	        /*onBeforeRefresh:function(){
	            $(this).pagination('loading');
	            alert('before refresh');
	            $(this).pagination('loaded');
	        }*/ 
	    }); 
});

window.onresize = function(){ 
	$('#dg').datagrid({width:frameWidth()}); 
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">
</div>
<div id="__ToolBar__">
<div style="margin:5px 0px 5px 0px;">
&nbsp;工程列表
<select id="appName" label="工程" name="appName" class="select" onchange="doQuery()"><%=pageBean.selectValue("appName")%></select>
&nbsp;间隔列表
<select id="timerRangeList" name="timerRangeList" onchange="resetAutoRefresh()">
<option value="2">2</option>
<option value="3">3</option>
<option value="5">5</option>
<option value="7">7</option>
<option value="10">10</option>
</select>秒
<input type="button" class="formbutton" name="refreshImmediateBtn" id="refreshImmediateBtn" value="立即刷新" onclick="immedateRefresh()" />
<input type="button" class="formbutton" name="trigerRefreshBtn" id="trigerRefreshBtn" value="停止自动刷新" onclick="trigerRefresh()" />
<input type="button" class="formbutton" name="resetMonitorBtn" id="resetMonitorBtn" value="重置监控" onclick="resetMonitor()" />
<span id="tipText">5秒钟后进行刷新操作...</span>
</div>
</div>
<table id="dg"></table>
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
