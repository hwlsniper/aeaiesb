<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>错误信息列表</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<style>
.activeA{
	color:red;	
}
.activeA:visited .noramlA:visited .noramlA:visited{
	color:red;	
}
.noramlA{
	color:black;
}
.noramlA:visited .noramlA:hover .noramlA:link {
	color:black;
}
.activeTextArea{
	display:block;
}
.noramlTextArea{
	display:none;
}
</style>
<script language="javascript">
function showError(i){
	$("#soapXML_"+i).hide();
	$("#errorMsg_"+i).show();	
	
	$("#errMsgA_"+i).removeClass();
	$("#errMsgA_"+i).addClass("activeA");
	
	$("#soapXMLA_"+i).removeClass();
	$("#soapXMLA_"+i).addClass("noramlA");	
}
function showSoapXML(i){
	$("#soapXML_"+i).show();
	$("#errorMsg_"+i).hide();
	
	$("#soapXMLA_"+i).removeClass();
	$("#soapXMLA_"+i).addClass("activeA");
	
	$("#errMsgA_"+i).removeClass();
	$("#errMsgA_"+i).addClass("noramlA");		
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<div style="height:500px;overflow:scroll;">
<%
if (pageBean.getRsList().size() > 0){
%>
<table class="detailTable" cellspacing="0" cellpadding="0">
<%
for (int i=0;i < pageBean.getRsList().size();i++){
%>
<tr>
	<td width="150" nowrap><%=pageBean.inputValue(i, "startTime")%>
	<br></br>NO.<%=i+1%>
	<br></br><a id="errMsgA_<%=i%>" class="activeA" href="javascript:showError('<%=i%>')">错误信息</a>&nbsp;&nbsp;<a id="soapXMLA_<%=i%>"  class="noramlA" href="javascript:showSoapXML('<%=i%>')">SOAP请求XML</a>
	</td>
	<td>
	<textarea class="activeTextArea" id="errorMsg_<%=i%>" name="errorMsg_<%=i%>" cols="70" rows="6" class="textarea"><%=pageBean.inputValue(i, "errorMsg")%></textarea>
	<textarea class="noramlTextArea" id="soapXML_<%=i%>" name="soapXML_<%=i%>" cols="70" rows="6" class="textarea"><%=pageBean.inputValue(i, "soapXML")%></textarea>
	</td>
</tr>
<%
}
%>
</table>
<%}else{%>
<div style="margin:10px;">没有错误信息！</div>
<%}%>
</div>
<input type="hidden" name="actionType" id="actionType" value="" />
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
