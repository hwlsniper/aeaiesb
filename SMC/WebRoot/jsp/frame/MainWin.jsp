<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8" errorPage="/jsp/frame/Error.jsp" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html>
<head>
<title>主页面</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<script type="text/javascript" src="js/jquery-1.4.2.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
.footer {
	height: 31px;
	line-height: 31px;
	font-size: 12px;
	text-align: center;
	color: #666666;
	width: 99%;
}
.mainContent{
	font-size: 14px;
	margin:8px 10px;
	line-height:25px;
}

div#wrap {
 padding-top: 117px;
}
</style>
</head>
<body>
<div id="wrap">
<table width="96%"  border="0" align="center">
  <tr>
    <td width="80" align="center"><img src="images/index/mainpic.jpg"></td>
    <td>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应用集成平台作为企业信息系统的“龙骨”来集成各业务系统，一般称之为企业服务总线（Enterprise Service BUS，ESB），应用集成平台跟上层的门户平台Portal产品、业务流程平台BPM产品协作的时候，通常作为服务提供者的角色出现，为Portal和BPM提供数据服务。
</div>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;应用集成平台可以做企业服务总线ESB来实现异构系统的对接（比如在业务财务一体化项目中，实现生产系统跟财务系统的凭证对接），也可以在数据整合/数据中心项目中，作为综合数据交换平台负责业务数据上传、汇聚，基础数据（主数据）的下发、分发。
</div>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在综合集成项目如：数据门户项目中，应用集成平台从业务系统实时获取相关数据以XML或者JSON格式为门户平台中各图表组件、表格组件提供数据服务，门户集成平台仅负责前端信息聚合、展现和交互。
</div>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
在业务流程重组项目中，为了实现跨系统业务流程重组，在流程中必须挂接展现异构系统表单数据，应用集成平台负责从各异构系统中动态获取表单数据，以JSON方式为BPM提供业务数据，同时在流程审批操作时候，应用集成平台来实现业务数据状态的回写，BPM本身不跟业务系统打交道，跟业务系统的交互都统一交付给应用集成平台来完成。
</div>
</td>
  </tr>
</table>
</div>
</body>
</html>
<script language="javascript">
<%
if (pageBean.isTrue(pageBean.inputValue("refreshTree"))){
%>
parent.frames["leftFrame"].location.reload();
<%
}
%>
if (parent.topright.document && parent.topright.document.getElementById('currentPath'))
parent.topright.document.getElementById('currentPath').innerHTML='系统主页面';
</script>
